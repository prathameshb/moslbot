function Api() {

    var proto = this;

    proto.getMenuList = function(context, key, data, callback) {
        const CONFIG = require('../index.js').CONFIG;
        var url = CONFIG.CONSTANTS.MENULIST_API;
        var params = "role=" + data.role + "&category=" + data.category + "&userid=" + data.userid + "&islive=1";
        var headers = {
            "sessionid": data.sessionid,
            "token": data.token,
            "Content-Type": "application/x-www-form-urlencoded"
        };
        const BotUtils = require('../index.js').BotUtils;
        BotUtils.httpPost(context, url, params, headers, function(response) {
            try {
                response = JSON.parse(response);
            } catch (error) {
                console.log(error);
            }
            var status = response.status;
            if (status && status.length && (status.toLowerCase() == "success" || status.toLowerCase() == "sucess")) {
                var menuList = response.menuList;
                if (menuList && menuList.length) {
                    return callback(menuList);
                }
            }
            return callback();
        });
    };

    proto.categoryTwoClientCodes = function (context, event, callback) {
        var roomleveldata = context.simpledb.roomleveldata;
        const CONFIG = require('../index.js').CONFIG;
        var url = CONFIG.CONSTANTS.CLIENTCODE_API;
        var headers = {
            "token": roomleveldata.token,
            "sessionid": roomleveldata.sessionid,
            "Content-Type": "application/x-www-form-urlencoded"
        };

        var params = "userid=" + roomleveldata.userid + "&userrole=" + roomleveldata.role;

        const BotUtils = require('../index.js').BotUtils;
        BotUtils.httpPost(context, url, params, headers, callback);
    };

    proto.categoryTwoApiCall = function(context, event, callback) {
        var roomleveldata = context.simpledb.roomleveldata;
        const CONFIG = require('../index.js').CONFIG;
        var url = decodeURIComponent(roomleveldata.baseurl + roomleveldata.endpoint);
        var headers = {
            "token": roomleveldata.token,
            "sessionid": roomleveldata.sessionid,
            "Content-Type": "application/x-www-form-urlencoded"
        };

        var params = "userId=" + roomleveldata.userid + "&clientCode=" + (roomleveldata.clientid || roomleveldata.userid)
            + "&userRole=" + roomleveldata.role + "&fromDate=" + roomleveldata.startDate
            + "&toDate=" + roomleveldata.endDate + "&returnType=" + roomleveldata.outputType;

        roomleveldata.url = url;
        roomleveldata.params = params;
        roomleveldata.headers = headers;
        const BotUtils = require('../index.js').BotUtils;
        BotUtils.httpPost(context, url, params, headers, callback);
    };

	proto.ledgerReport = function(context, event, callback) {
        var roomleveldata = context.simpledb.roomleveldata;
        const CONFIG = require('../index.js').CONFIG;
        var url = decodeURIComponent(roomleveldata.baseurl + CONFIG.CONSTANTS.FUNCTIONS["ledgerreport"].endpoint);
        var headers = {
            "token": roomleveldata.token,
            "sessionid": roomleveldata.sessionid,
            "Content-Type": "application/x-www-form-urlencoded"
        };

        var params = "userid=" + roomleveldata.userid + "&clientcode=" + (roomleveldata.clientid || roomleveldata.userid)
        	+ "&userrole=" + roomleveldata.role;

        roomleveldata.url = url;
        roomleveldata.params = params;
        roomleveldata.headers = headers;
        const BotUtils = require('../index.js').BotUtils;
        BotUtils.httpPost(context, url, params, headers, callback);
    };
    
    proto.ledgerStatement = function(context, event, callback) {
        var roomleveldata = context.simpledb.roomleveldata;
        const CONFIG = require('../index.js').CONFIG;
        var url = decodeURIComponent(roomleveldata.baseurl + CONFIG.CONSTANTS.FUNCTIONS["ledger"].endpoint);
        var headers = {
            "token": roomleveldata.token,
            "sessionid": roomleveldata.sessionid,
            "Content-Type": "application/x-www-form-urlencoded"
        };

        var params = "userid=" + roomleveldata.userid + "&clientid=" + (roomleveldata.clientid || roomleveldata.userid);

        params += "&role=" + roomleveldata.role 
            + "&EXCHANGE_SEG=" + roomleveldata.segment 
            + "&FROM_DATE=" + roomleveldata.startDate + "&TO_DATE=" + roomleveldata.endDate
            + "&RETURN_TYPE=" + roomleveldata.outputType;
        
        roomleveldata.url = url;
        roomleveldata.params = params;
        roomleveldata.headers = headers;
        const BotUtils = require('../index.js').BotUtils;
        BotUtils.httpPost(context, url, params, headers, callback);
    };

    proto.defaultDpStatement = function(context, event, callback) {
        var roomleveldata = context.simpledb.roomleveldata;
        const CONFIG = require('../index.js').CONFIG;
        var url = decodeURIComponent(roomleveldata.baseurl + CONFIG.CONSTANTS.FUNCTIONS["dp"].endpoint);
        var headers = {
            "token": roomleveldata.token,
            "sessionid": roomleveldata.sessionid,
            "Content-Type": "application/x-www-form-urlencoded"
        };

        var params = "userid=" + roomleveldata.userid + "&clientid=" + (roomleveldata.clientid || roomleveldata.userid);

        if (roomleveldata.segment == "H") {
            var date = new Date();//today's date
            var dateString = (date.getMonth()+1) + "/" + date.getDate() + "/" + date.getFullYear();
            roomleveldata.startDate = dateString;
            roomleveldata.endDate = dateString;
            params += "&role=" + roomleveldata.role + "&StatementType=" + roomleveldata.segment 
                + "&FROM_DATE=" + dateString + "&TO_DATE=" + dateString;
        } else if (roomleveldata.segment == "T") {
            const Utils = require('../index.js').Utils;
            Utils.period(context, event, -91);//last 3 months date range
            params += "&role=" + roomleveldata.role + "&StatementType=" + roomleveldata.segment 
                + "&FROM_DATE=" + roomleveldata.startDate + "&TO_DATE=" + roomleveldata.endDate;
        }

        params = params + "&RETURN_TYPE=Link";
        const BotUtils = require('../index.js').BotUtils;
        BotUtils.httpPost(context, url, params, headers, callback);
    };

    proto.dpStatement = function(context, event, callback) {
        var roomleveldata = context.simpledb.roomleveldata;
        const CONFIG = require('../index.js').CONFIG;
        var url = decodeURIComponent(roomleveldata.baseurl + CONFIG.CONSTANTS.FUNCTIONS["dp"].endpoint);
        var headers = {
            "token": roomleveldata.token,
            "sessionid": roomleveldata.sessionid,
            "Content-Type": "application/x-www-form-urlencoded"
        };

        var params = "userid=" + roomleveldata.userid + "&clientid=" + (roomleveldata.clientid || roomleveldata.userid);

        params += "&role=" + roomleveldata.role + "&StatementType=" + roomleveldata.segment 
                + "&FROM_DATE=" + roomleveldata.startDate + "&TO_DATE=" + roomleveldata.endDate
                + "&RETURN_TYPE=" + roomleveldata.outputType;
        
        roomleveldata.url = url;
        roomleveldata.params = params;
        roomleveldata.headers = headers;
        const BotUtils = require('../index.js').BotUtils;
        BotUtils.httpPost(context, url, params, headers, callback);
    };

    proto.sttCertificate = function(context, event, callback) {
        var roomleveldata = context.simpledb.roomleveldata;
        const CONFIG = require('../index.js').CONFIG;
		var url = decodeURIComponent(roomleveldata.baseurl + CONFIG.CONSTANTS.FUNCTIONS["stt"].endpoint);
		var headers = {
			"sessionid": roomleveldata.sessionid,
			"token": roomleveldata.token,
			"Content-Type": "application/x-www-form-urlencoded"
		};
        
        var params = "userid=" + roomleveldata.userid + "&clientid=" + (roomleveldata.clientid || roomleveldata.userid);
        
        params += "&role=" + roomleveldata.role + "&FROM_DATE=FROM_DATE&TO_DATE=TO_DATE&Exch_Type=Exch_Type&Return_Type=Link";
		roomleveldata.url = url;
        roomleveldata.params = params;
        roomleveldata.headers = headers;
        const BotUtils = require('../index.js').BotUtils;
        BotUtils.httpPost(context, url, params, headers, callback);
    };

    proto.clientDetails = function(context, event, callback) {
        var roomleveldata = context.simpledb.roomleveldata;
        const CONFIG = require('../index.js').CONFIG;
		var url = decodeURIComponent(roomleveldata.baseurl + CONFIG.CONSTANTS.FUNCTIONS["myDetails"].endpoint);
		var headers = {
			"sessionid": roomleveldata.sessionid,
			"token": roomleveldata.token,
			"Content-Type": "application/x-www-form-urlencoded"
		};
		var params = "userid=" + roomleveldata.userid + "&clientid=" + roomleveldata.clientid + "&role=" + roomleveldata.role;
        roomleveldata.url = url;
        roomleveldata.params = params;
        roomleveldata.headers = headers;
        const BotUtils = require('../index.js').BotUtils;
        BotUtils.httpPost(context, url, params, headers, callback);
    };

    proto.clientDetailsTwo = function(context, event, callback) {
        var roomleveldata = context.simpledb.roomleveldata;
        const CONFIG = require('../index.js').CONFIG;
        var url = decodeURIComponent(roomleveldata.baseurl + CONFIG.CONSTANTS.FUNCTIONS["myDetailsTwo"].endpoint);
        var headers = {
            "sessionid": roomleveldata.sessionid,
            "token": roomleveldata.token,
            "Content-Type": "application/x-www-form-urlencoded"
        };
        var params = "userid=" + roomleveldata.userid + "&clientcode=" + roomleveldata.clientid + "&userrole=" + roomleveldata.role;
        roomleveldata.url = url;
        roomleveldata.params = params;
        roomleveldata.headers = headers;
        const BotUtils = require('../index.js').BotUtils;
        BotUtils.httpPost(context, url, params, headers, callback);
    };

    proto.cmrReport = function(context, event, callback) {
        var roomleveldata = context.simpledb.roomleveldata;
        const CONFIG = require('../index.js').CONFIG;
		var url = decodeURIComponent(roomleveldata.baseurl + CONFIG.CONSTANTS.FUNCTIONS["cmr"].endpoint);
		var headers = {
			"sessionid": roomleveldata.sessionid,
			"token": roomleveldata.token,
			"Content-Type": "application/x-www-form-urlencoded"
		};
		var params = "userid=" + roomleveldata.userid + "&clientid=" + roomleveldata.clientid + "&role=" + roomleveldata.role;
        roomleveldata.url = url;
        roomleveldata.params = params;
        roomleveldata.headers = headers;
        const BotUtils = require('../index.js').BotUtils;
        BotUtils.httpPost(context, url, params, headers, callback);
    };

    proto.cmrReportTwo = function(context, event, callback) {
        var roomleveldata = context.simpledb.roomleveldata;
        const CONFIG = require('../index.js').CONFIG;
        var url = decodeURIComponent(roomleveldata.baseurl + CONFIG.CONSTANTS.FUNCTIONS["cmrReportTwo"].endpoint);
        var headers = {
            "sessionid": roomleveldata.sessionid,
            "token": roomleveldata.token,
            "Content-Type": "application/x-www-form-urlencoded"
        };
        var params = "userid=" + roomleveldata.userid + "&clientcode=" + roomleveldata.clientid + "&userrole=" + roomleveldata.role;
        roomleveldata.url = url;
        roomleveldata.params = params;
        roomleveldata.headers = headers;
        const BotUtils = require('../index.js').BotUtils;
        BotUtils.httpPost(context, url, params, headers, callback);
    };

    proto.marginShortage = function(context, event, callback) {
        var roomleveldata = context.simpledb.roomleveldata;
        const CONFIG = require('../index.js').CONFIG;
		var url = decodeURIComponent(roomleveldata.baseurl + CONFIG.CONSTANTS.FUNCTIONS["margin"].endpoint);
		var headers = {
			"sessionid": roomleveldata.sessionid,
			"token": roomleveldata.token,
			"Content-Type": "application/x-www-form-urlencoded"
		};
        var params = "userid=" + roomleveldata.userid + "&clientid=" + roomleveldata.clientid 
            + "&role=" + roomleveldata.role + "&segment=E";
        roomleveldata.url = url;
        roomleveldata.params = params;
        roomleveldata.headers = headers;
        const BotUtils = require('../index.js').BotUtils;
        BotUtils.httpPost(context, url, params, headers, callback);
    };

    proto.activeIpo = function(context, event, callback) {
        var roomleveldata = context.simpledb.roomleveldata;
        const CONFIG = require('../index.js').CONFIG;
		var url = decodeURIComponent(roomleveldata.baseurl + CONFIG.CONSTANTS.FUNCTIONS["ipo"].endpoint);
		var headers = {
			"sessionid": roomleveldata.sessionid,
			"token": roomleveldata.token,
			"Content-Type": "application/x-www-form-urlencoded"
		};
        var params = "userid=" + roomleveldata.userid + "&clientid=" + roomleveldata.clientid 
            + "&role=" + roomleveldata.role;
        roomleveldata.url = url;
        roomleveldata.params = params;
        roomleveldata.headers = headers;
        const BotUtils = require('../index.js').BotUtils;
        BotUtils.httpPost(context, url, params, headers, callback);
    };

    proto.accountClosure = function(context, event, callback) {
        var roomleveldata = context.simpledb.roomleveldata;
        const CONFIG = require('../index.js').CONFIG;
		var url = decodeURIComponent(roomleveldata.baseurl + CONFIG.CONSTANTS.FUNCTIONS["accountclosure"].endpoint);
		var headers = {
			"sessionid": roomleveldata.sessionid,
			"token": roomleveldata.token,
			"Content-Type": "application/x-www-form-urlencoded"
		};
        var params = "userid=" + roomleveldata.userid + "&clientid=" + roomleveldata.clientid 
            + "&role=" + roomleveldata.role;
        roomleveldata.url = url;
        roomleveldata.params = params;
        roomleveldata.headers = headers;
        const BotUtils = require('../index.js').BotUtils;
        BotUtils.httpPost(context, url, params, headers, callback);
    };

    proto.disdrfStatus = function(context, event, callback) {
        var roomleveldata = context.simpledb.roomleveldata;
        const CONFIG = require('../index.js').CONFIG;
		var url = decodeURIComponent(roomleveldata.baseurl + CONFIG.CONSTANTS.FUNCTIONS["disdrfstatus"].endpoint);
		var headers = {
			"sessionid": roomleveldata.sessionid,
			"token": roomleveldata.token,
			"Content-Type": "application/x-www-form-urlencoded"
		};
        var params = "userid=" + roomleveldata.userid + "&clientid=" + roomleveldata.clientid 
            + "&role=" + roomleveldata.role + "&requestType=2" + "&refNo=" + roomleveldata.refNo;
        roomleveldata.url = url;
        roomleveldata.params = params;
        roomleveldata.headers = headers;
        const BotUtils = require('../index.js').BotUtils;
        BotUtils.httpPost(context, url, params, headers, callback);
    };

    proto.mutualFund = function(context, event, callback) {
        var roomleveldata = context.simpledb.roomleveldata;
        const CONFIG = require('../index.js').CONFIG;
		var url = decodeURIComponent(roomleveldata.baseurl + CONFIG.CONSTANTS.FUNCTIONS["mf"].endpoint);
		var headers = {
			"sessionid": roomleveldata.sessionid,
			"token": roomleveldata.token,
			"Content-Type": "application/x-www-form-urlencoded"
		};
        var params = "userid=" + roomleveldata.userid + "&clientid=" + roomleveldata.clientid 
            + "&role=" + roomleveldata.role + "&portFolioNo=" + roomleveldata.portFolioNo;
        roomleveldata.url = url;
        roomleveldata.params = params;
        roomleveldata.headers = headers;
        const BotUtils = require('../index.js').BotUtils;
        BotUtils.httpPost(context, url, params, headers, callback);
    };

    proto.profitAndLossStatement = function(context, event, callback) {
        var roomleveldata = context.simpledb.roomleveldata;
        const CONFIG = require('../index.js').CONFIG;
		var url = decodeURIComponent(roomleveldata.baseurl + CONFIG.CONSTANTS.FUNCTIONS["profitandloss"].endpoint);
		var headers = {
			"sessionid": roomleveldata.sessionid,
			"token": roomleveldata.token,
			"Content-Type": "application/x-www-form-urlencoded"
		};
        var params = "userid=" + roomleveldata.userid + "&clientcode=" + roomleveldata.clientid 
            + "&userrole=" + roomleveldata.role + "&fromDate=" + roomleveldata.startDate + "&toDate=" + roomleveldata.endDate
            + "&returnType=" + roomleveldata.outputType;
        roomleveldata.url = url;
        roomleveldata.params = params;
        roomleveldata.headers = headers;
        const BotUtils = require('../index.js').BotUtils;
        BotUtils.httpPost(context, url, params, headers, callback);
    };

    proto.dashboard = function(context, event, callback) {
        var roomleveldata = context.simpledb.roomleveldata;
        const CONFIG = require('../index.js').CONFIG;
		var url = decodeURIComponent(roomleveldata.baseurl + CONFIG.CONSTANTS.FUNCTIONS["dashboard"].endpoint);
		var headers = {
			"sessionid": roomleveldata.sessionid,
			"token": roomleveldata.token,
			"Content-Type": "application/x-www-form-urlencoded"
		};
        var params = "userid=" + roomleveldata.userid + "&clientid=" + roomleveldata.clientid 
            + "&role=" + roomleveldata.role;
        roomleveldata.url = url;
        roomleveldata.params = params;
        roomleveldata.headers = headers;
        const BotUtils = require('../index.js').BotUtils;
        BotUtils.httpPost(context, url, params, headers, callback);
    };

    proto.fundPayoutStatus = function(context, event, callback) {
        var roomleveldata = context.simpledb.roomleveldata;
        const CONFIG = require('../index.js').CONFIG;
		var url = decodeURIComponent(roomleveldata.baseurl + CONFIG.CONSTANTS.FUNCTIONS["fundpayoutstatus"].endpoint);
		var headers = {
			"sessionid": roomleveldata.sessionid,
			"token": roomleveldata.token,
			"Content-Type": "application/x-www-form-urlencoded"
		};
        var params = "userid=" + roomleveldata.userid + "&clientid=" + roomleveldata.clientid 
            + "&role=" + roomleveldata.role;
        roomleveldata.url = url;
        roomleveldata.params = params;
        roomleveldata.headers = headers;
        const BotUtils = require('../index.js').BotUtils;
        BotUtils.httpPost(context, url, params, headers, callback);
    };

    proto.fundTransferStatus = function(context, event, callback) {
        var roomleveldata = context.simpledb.roomleveldata;
        const CONFIG = require('../index.js').CONFIG;
		var url = decodeURIComponent(roomleveldata.baseurl + CONFIG.CONSTANTS.FUNCTIONS["fundtransferstatus"].endpoint);
		var headers = {
			"sessionid": roomleveldata.sessionid,
			"token": roomleveldata.token,
			"Content-Type": "application/x-www-form-urlencoded"
		};
        var params = "userid=" + roomleveldata.userid + "&clientcode=" + roomleveldata.clientid 
            + "&userrole=" + roomleveldata.role;
        roomleveldata.url = url;
        roomleveldata.params = params;
        roomleveldata.headers = headers;
        const BotUtils = require('../index.js').BotUtils;
        BotUtils.httpPost(context, url, params, headers, callback);
    };

    proto.formatCheckList = function(context, event, callback) {
        var roomleveldata = context.simpledb.roomleveldata;
        const CONFIG = require('../index.js').CONFIG;
		var url = decodeURIComponent(roomleveldata.baseurl + CONFIG.CONSTANTS.FUNCTIONS["formatchecklist"].endpoint);
		var headers = {
			"sessionid": roomleveldata.sessionid,
			"token": roomleveldata.token,
			"Content-Type": "application/x-www-form-urlencoded"
		};
        var params = "userid=" + roomleveldata.userid + "&clientid=" + (roomleveldata.clientid || roomleveldata.userid) 
            + "&role=" + roomleveldata.role + "&documentType=" + roomleveldata.documentType;
        roomleveldata.url = url;
        roomleveldata.params = params;
        roomleveldata.headers = headers;
        const BotUtils = require('../index.js').BotUtils;
        BotUtils.httpPost(context, url, params, headers, callback);
    };

    proto.lastThreeContractNote = function(context, event, callback) {
        var roomleveldata = context.simpledb.roomleveldata;
        const CONFIG = require('../index.js').CONFIG;
		var url = decodeURIComponent(roomleveldata.baseurl + CONFIG.CONSTANTS.FUNCTIONS["cn"].endpoint);
		var headers = {
			"sessionid": roomleveldata.sessionid,
			"token": roomleveldata.token,
			"Content-Type": "application/x-www-form-urlencoded"
		};
        var params = "userid=" + roomleveldata.userid + "&clientid=" + roomleveldata.clientid 
            + "&role=" + roomleveldata.role + "&FROM_DATE=&TO_DATE=&RETURN_TYPE=Link";
        roomleveldata.url = url;
        roomleveldata.params = params;
        roomleveldata.headers = headers;
        const BotUtils = require('../index.js').BotUtils;
        BotUtils.httpPost(context, url, params, headers, callback);
    };

    proto.contractNote = function(context, event, callback) {
        var roomleveldata = context.simpledb.roomleveldata;
        const CONFIG = require('../index.js').CONFIG;
		var url = decodeURIComponent(roomleveldata.baseurl + CONFIG.CONSTANTS.FUNCTIONS["cn"].endpoint);
		var headers = {
			"sessionid": roomleveldata.sessionid,
			"token": roomleveldata.token,
			"Content-Type": "application/x-www-form-urlencoded"
		};
        var params = "userid=" + roomleveldata.userid + "&clientid=" + roomleveldata.clientid 
            + "&role=" + roomleveldata.role + "&FROM_DATE=" + roomleveldata.startDate 
            + "&TO_DATE=" + roomleveldata.endDate + "&RETURN_TYPE=" + roomleveldata.outputType;
        roomleveldata.url = url;
        roomleveldata.params = params;
        roomleveldata.headers = headers;
        const BotUtils = require('../index.js').BotUtils;
        BotUtils.httpPost(context, url, params, headers, callback);
    };

    proto.saudaDetail = function(context, event, callback) {
        var roomleveldata = context.simpledb.roomleveldata;
        const CONFIG = require('../index.js').CONFIG;
		var url = decodeURIComponent(roomleveldata.baseurl + CONFIG.CONSTANTS.FUNCTIONS["sauda"].endpoint);
		var headers = {
			"sessionid": roomleveldata.sessionid,
			"token": roomleveldata.token,
			"Content-Type": "application/x-www-form-urlencoded"
		};
        var params = "userid=" + roomleveldata.userid + "&clientcode=" + roomleveldata.clientid 
            + "&userrole=" + roomleveldata.role + "&fromDate=" + roomleveldata.startDate 
            + "&toDate=" + roomleveldata.endDate + "&returnType=" + roomleveldata.outputType;
        roomleveldata.url = url;
        roomleveldata.params = params;
        roomleveldata.headers = headers;
        const BotUtils = require('../index.js').BotUtils;
        BotUtils.httpPost(context, url, params, headers, callback);
    };
    
    proto.orderstatus = function(context, event, callback) {
        var roomleveldata = context.simpledb.roomleveldata;
        const CONFIG = require('../index.js').CONFIG;
		var url = decodeURIComponent(roomleveldata.baseurl + CONFIG.CONSTANTS.FUNCTIONS["orderstatus"].endpoint);
		var headers = {
			"sessionid": roomleveldata.sessionid,
			"token": roomleveldata.token,
			"Content-Type": "application/x-www-form-urlencoded"
		};
        var params = "userid=" + roomleveldata.userid + "&clientcode=" + roomleveldata.clientid 
            + "&userrole=" + roomleveldata.role + "&orderType=" + roomleveldata.orderType;
        roomleveldata.url = url;
        roomleveldata.params = params;
        roomleveldata.headers = headers;
        const BotUtils = require('../index.js').BotUtils;
        BotUtils.httpPost(context, url, params, headers, callback);
    };
      proto.disbookoutput = function(context, event, callback) {
        var roomleveldata = context.simpledb.roomleveldata;
        const CONFIG = require('../index.js').CONFIG;
		var url = decodeURIComponent(roomleveldata.baseurl + CONFIG.CONSTANTS.FUNCTIONS["disbook"].endpoint);
		var headers = {
			"sessionid": roomleveldata.sessionid,
			"token": roomleveldata.token,
			"Content-Type": "application/x-www-form-urlencoded"
		};
        var params = "userid=" + roomleveldata.userid + "&clientcode=" + roomleveldata.clientid 
            + "&userrole=" + roomleveldata.role + "&dpid=" + roomleveldata.dpid;
        roomleveldata.url = url;
        roomleveldata.params = params;
        roomleveldata.headers = headers;
        const BotUtils = require('../index.js').BotUtils;
        BotUtils.httpPost(context, url, params, headers, callback);
    };
    
    proto.marginShortagePenalty = function(context, event, callback) {
        var roomleveldata = context.simpledb.roomleveldata;
        const CONFIG = require('../index.js').CONFIG;
		var url = decodeURIComponent(roomleveldata.baseurl + CONFIG.CONSTANTS.FUNCTIONS["marginshortagepenalty"].endpoint);
		var headers = {
			"sessionid": roomleveldata.sessionid,
			"token": roomleveldata.token,
			"Content-Type": "application/x-www-form-urlencoded"
		};
        var params = "userid=" + roomleveldata.userid + "&clientid=" + roomleveldata.clientid 
            + "&role=" + roomleveldata.role + "&fromDate=" + roomleveldata.startDate
            + "&toDate=" + roomleveldata.endDate + "&returnType=" + roomleveldata.outputType;
        roomleveldata.url = url;
        roomleveldata.params = params;
        roomleveldata.headers = headers;
        const BotUtils = require('../index.js').BotUtils;
        BotUtils.httpPost(context, url, params, headers, callback);
    };

     proto.hpsoutput = function(context, event, callback) {
     	//hit packet status api
        var roomleveldata = context.simpledb.roomleveldata;
        const CONFIG = require('../index.js').CONFIG;
		var url = decodeURIComponent(roomleveldata.baseurl + CONFIG.CONSTANTS.FUNCTIONS["hps"].endpoint);
		var headers = {
			"sessionid": roomleveldata.sessionid,
			"token": roomleveldata.token,
			"Content-Type": "application/x-www-form-urlencoded"
		};
        var params = "userid=" + roomleveldata.userid + "&clientid=" + (roomleveldata.clientid || roomleveldata.userid)  
            + "&role=" + roomleveldata.role + "&packetNo=" + roomleveldata.packetNo;
		roomleveldata.url = url;
        roomleveldata.params = params;
        roomleveldata.headers = headers;
        const BotUtils = require('../index.js').BotUtils;
        BotUtils.httpPost(context, url, params, headers, callback);
    };
    
    proto.quickLinks = function(context, event, callback) {
        var roomleveldata = context.simpledb.roomleveldata;
        const CONFIG = require('../index.js').CONFIG;
        var url = decodeURIComponent(roomleveldata.baseurl + CONFIG.CONSTANTS.FUNCTIONS["quicklinks"].endpoint);
        var headers = {
            "sessionid": roomleveldata.sessionid,
            "token": roomleveldata.token,
            "Content-Type": "application/x-www-form-urlencoded"
        };
        var params = "userid=" + roomleveldata.userid + "&clientcode=" + (roomleveldata.clientid || roomleveldata.userid) 
            + "&userrole=" + roomleveldata.role + "&orderType=" + roomleveldata.orderType;
        roomleveldata.url = url;
        roomleveldata.params = params;
        roomleveldata.headers = headers;
        const BotUtils = require('../index.js').BotUtils;
        BotUtils.httpPost(context, url, params, headers, callback);
    };
    //account status api
    proto.accountstatusoutput = function(context, event, callback) {
        var roomleveldata = context.simpledb.roomleveldata;
        const CONFIG = require('../index.js').CONFIG;
        var url = decodeURIComponent(roomleveldata.baseurl + CONFIG.CONSTANTS.FUNCTIONS["accountstatus"].endpoint);
        var headers = {
            "sessionid": roomleveldata.sessionid,
            "token": roomleveldata.token,
            "Content-Type": "application/x-www-form-urlencoded"
        };
        var params = "userid=" + roomleveldata.userid + "&clientid=" + roomleveldata.clientid 
            + "&role=" + roomleveldata.role;
        roomleveldata.url = url;
        roomleveldata.params = params;
        roomleveldata.headers = headers;
        const BotUtils = require('../index.js').BotUtils;
        BotUtils.httpPost(context, url, params, headers, callback);
    };
    //new accout opening
     proto.accountoutput = function(context, event, callback) {
        var roomleveldata = context.simpledb.roomleveldata;
        const CONFIG = require('../index.js').CONFIG;
        var url = decodeURIComponent(roomleveldata.baseurl + CONFIG.CONSTANTS.FUNCTIONS["account"].endpoint);
        var headers = {
            "sessionid": roomleveldata.sessionid,
            "token": roomleveldata.token,
            "Content-Type": "application/x-www-form-urlencoded"
        };
        var params = "userid=" + roomleveldata.userid + "&clientid=" + (roomleveldata.clientid || roomleveldata.userid)
            + "&role=" + roomleveldata.role +"&documentType=1";
        roomleveldata.url = url;
        roomleveldata.params = params;
        roomleveldata.headers = headers;
        const BotUtils = require('../index.js').BotUtils;
        BotUtils.httpPost(context, url, params, headers, callback);
    };
    
    proto.validateClientDetails = function(context, event, callback) {
        var roomleveldata = context.simpledb.roomleveldata;
        const CONFIG = require('../index.js').CONFIG;
		var url = decodeURIComponent(roomleveldata.baseurl + CONFIG.CONSTANTS.FUNCTIONS["myDetails"].endpoint);
		var headers = {
			"sessionid": roomleveldata.sessionid,
			"token": roomleveldata.token,
			"Content-Type": "application/x-www-form-urlencoded"
		};
		var params = "userid=" + roomleveldata.userid + "&clientid=" + roomleveldata.clientid + "&role=" + roomleveldata.role;
       // roomleveldata.url = url;
        //roomleveldata.params = params;
        //roomleveldata.headers = headers;
        const BotUtils = require('../index.js').BotUtils;
        BotUtils.httpPost(context, url, params, headers, function(data){
        	
        	if(roomleveldata.clientid=="H10765"){
        		
        		console.log("INVALID CLIENT DATA---------->",data);
        	}
        	console.log("DATA----->",data,typeof data);
        	if(typeof data=="string"){
        		data=JSON.parse(data);
        	}
        	if(data.status.toLowerCase()=="success" || data.status.toLowerCase()=="sucess" ){
        		
        		return callback(true);
        		
        	}
        	else{
        		return callback(false);
        	}
        });
    };

	  proto.holdingStatement = function(context, event, callback) {
        var roomleveldata = context.simpledb.roomleveldata;
        const CONFIG = require('../index.js').CONFIG;
        var url = decodeURIComponent(roomleveldata.baseurl + CONFIG.CONSTANTS.FUNCTIONS["holdingStatement"].endpoint);
        var headers = {
            "token": roomleveldata.token,
            "sessionid": roomleveldata.sessionid,
            "Content-Type": "application/x-www-form-urlencoded"
        };

        var params = "userid=" + roomleveldata.userid + "&clientcode=" + roomleveldata.clientid 
		params += "&userrole=" + roomleveldata.role + "&fromDate=" + roomleveldata.startDate + "&toDate=" + roomleveldata.endDate ;
		params+="&returnType="+roomleveldata.outputType;
        
        roomleveldata.url = url;
        roomleveldata.params = params;
        roomleveldata.headers = headers;
        const BotUtils = require('../index.js').BotUtils;
        BotUtils.httpPost(context, url, params, headers, callback);
    };
    
      proto.transactionStatement = function(context, event, callback) {
        var roomleveldata = context.simpledb.roomleveldata;
        const CONFIG = require('../index.js').CONFIG;
        var url = decodeURIComponent(roomleveldata.baseurl + CONFIG.CONSTANTS.FUNCTIONS["transactionStatement"].endpoint);
        var headers = {
            "token": roomleveldata.token,
            "sessionid": roomleveldata.sessionid,
            "Content-Type": "application/x-www-form-urlencoded"
        };

        var params = "userid=" + roomleveldata.userid + "&clientcode=" + roomleveldata.clientid ;
		params += "&userrole=" + roomleveldata.role + "&fromDate=" + roomleveldata.startDate + "&toDate=" + roomleveldata.endDate ;
		params+="&returnType="+roomleveldata.outputType;
        
        roomleveldata.url = url;
        roomleveldata.params = params;
        roomleveldata.headers = headers;
        const BotUtils = require('../index.js').BotUtils;
        BotUtils.httpPost(context, url, params, headers, callback);
    };
}

module.exports = new Api();