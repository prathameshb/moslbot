const botRouter = require('gupshup-utils').BotRoutes();
const moment = require('moment');
const async = require('async');
//Setting up credentials
const ENV_CONFIG=require('../config').ENV_CONFIG;
botRouter.on('post', '/setCreds', function(context, event) {
    var headers = event.headers;
    var params = event.params;
    const CONFIG = require('../index.js').CONFIG;
    var FAILURE_RESPONSE = CONFIG.CONSTANTS.SETCREDS_FAIL;
    var RESPONSE_TYPE = "application/json";
    console.log("SET CREDS CALLED----->");
    if (headers.authorization) {
        if (headers.authorization == CONFIG.CONSTANTS.AUTH_KEY) {
            if (!params.sessionid) {
                context.sendResponse(FAILURE_RESPONSE, RESPONSE_TYPE);
            }
            if (!params.token) {
                context.sendResponse(FAILURE_RESPONSE, RESPONSE_TYPE);
            }
            if (!params.role) {
                context.sendResponse(FAILURE_RESPONSE, RESPONSE_TYPE);
            }
            if (!params.userid) {
                context.sendResponse(FAILURE_RESPONSE, RESPONSE_TYPE);
            }
            if (!params.baseurl) {
                context.sendResponse(FAILURE_RESPONSE, RESPONSE_TYPE);
            }
            if (!params.category) {
                context.sendResponse(FAILURE_RESPONSE, RESPONSE_TYPE);
            }
            var sessionid = params.sessionid;
            //For V3
           // var db_key = "room:" + sessionid + "_" + ENV_CONFIG.BOT.botkey;
            var db_key = "room:" + sessionid;
            const BotUtils = require('../index.js').BotUtils;
            console.log("KEY---------->",db_key);
            BotUtils.setCreds(context, db_key, params, function() {
                var success = CONFIG.CONSTANTS.SETCREDS_SUCCESS;
                success.message = success.message.replace("__sessionid", db_key);
                console.log('success...', success);
                context.sendResponse(success, RESPONSE_TYPE);
            });
        } else {
            context.sendResponse(FAILURE_RESPONSE, RESPONSE_TYPE);
        }
    } else {
        context.sendResponse(FAILURE_RESPONSE, RESPONSE_TYPE);
    }
});
//Deleting credentials
botRouter.on('post', '/delCreds', function(context, event) {
    var headers = event.headers;
    var params = event.params;
    const CONFIG = require('../index.js').CONFIG;
    var FAILURE_RESPONSE = CONFIG.CONSTANTS.DELCREDS_FAIL;
    var RESPONSE_TYPE = "application/json";
    if (headers.authorization) {
        if (headers.authorization == CONFIG.CONSTANTS.AUTH_KEY) {
            if (!params.sessionid) {
                context.sendResponse(FAILURE_RESPONSE,RESPONSE_TYPE); 
            }
            var sessionid = params.sessionid;
            //var db_key = "room:" + sessionid + "_" + ENV_CONFIG.BOT.botkey;
            var db_key = "room:" + sessionid;
            const BotUtils = require('../index.js').BotUtils;
            console.log("KEY---------->",db_key);
            BotUtils.delCreds(context, db_key, function() {
                var success = CONFIG.CONSTANTS.DELCREDS_SUCCESS;
                success.message = success.message.replace("__sessionid", db_key);
                console.log('success...', success);
                context.sendResponse(success, RESPONSE_TYPE);
            });
        }
        else {
            context.sendResponse(FAILURE_RESPONSE,RESPONSE_TYPE);
        }
    }
    else {
        context.sendResponse(FAILURE_RESPONSE,RESPONSE_TYPE);
    }
});
//sendmail
botRouter.on('post', '/sendMail', function(context,event){
    var headers = event.headers;
    var params = event.params;
    const CONFIG = require('../index.js').CONFIG;
    var FAILURE_RESPONSE = CONFIG.CONSTANTS.SENDMAIL_FAIL;
    var RESPONSE_TYPE = "application/json";
    if (headers.authorization) {
        if (headers.authorization == CONFIG.CONSTANTS.AUTH_KEY) {
            const BotUtils = require('../index.js').BotUtils;
            BotUtils.sendMailWithAttachment(context, function(status, message){
                context.sendResponse({"status": status, "message": message}, "application/json");
            });
        }
        else {
            context.sendResponse(FAILURE_RESPONSE,RESPONSE_TYPE);
        }
    }
    else {
        context.sendResponse(FAILURE_RESPONSE,RESPONSE_TYPE);
    }
});
//Get Nlp FAILURE_RESPONSE
botRouter.on('get', '/getNlpFailureResp', function(context, event) {

	let params = event.params;
	let headers = event.headers;
	const CONFIG = require('../index.js').CONFIG;
	if (headers.authorization) {
		if (headers.authorization != CONFIG.CONSTANTS.AUTH_KEY) {
			var response = {

				"error": "Unauthorised access",
				"status": "400"

			}
			context.sendResponse(response, "application/json");

		} else if (params["from"] != undefined && params["from"] != "" && params["from"] != null) {

			let fromdate = event.params["from"];
			var pattern = /^([0-9]{2})-([0-9]{2})-([0-9]{4})$/;
			if (pattern.test(fromdate)) {

				if (params["to"] == undefined || params["to"] == "" || params["to"] == null) {

					var db_key = fromdate + "_F";
					context.simpledb.doGet(db_key, function(context_, event_) {

						var db_val = event_.dbval;
						var response = {

							"status": "200",
							"result": db_val["nlpFailureResult"]


						}

						context.sendResponse(response, "application/json");

					});


				} else {
					let todate = event.params["to"];
					let startdate = event.params["from"];
					if (pattern.test(todate)) {

						var result = {};

						var lastdate = moment(todate, "DD-MM-YYYY").add(1, 'days').format("DD-MM-YYYY");
						var db_keyarr = [];
						do {

							db_keyarr.push(startdate + "_F");
							startdate = moment(startdate, "DD-MM-YYYY").add(1, 'days').format("DD-MM-YYYY")
						//	console.log("START DATE",startdate,typeof startdate,typeof lastdate,lastdate);	

						}
						while (startdate != lastdate);
					//	console.log("db_keyarr---->",db_keyarr);
						async.each(db_keyarr, function(db_key, callback) {
						
						  //  console.log("DBKEY",db_key);
							context.simpledb.doGet(db_key, function(c, e) {

								var data = e.dbval;
							  //  console.log("DATA->>>>>>>>>>>>",data);
							    var datekey=db_key.split("_")[0];
								if (data) {
								
									if (data["nlpFailureResult"]) {

										result[datekey] = data["nlpFailureResult"];
										callback();

									} else {

										result[datekey] = [];
										callback();

									}


								}
								else{
									
									result[datekey] = [];
									callback();
									
								}
							});


						}, function(err) {
							if (err) {

								console.log('err in getNlpFailureResp resp');
							} else {
								console.log('successfully added Nlp failure data');
								var response = {
									"status": "200",
									"result": result

								}
								context.sendResponse(response, "application/json");
							}
						});



					} else {
						var response = {

							"error": "Parameter 'to' should be of the format dd-mm-yyyy",
							"status": "400"

						}
						context.sendResponse(response, "application/json");

					}



				}


			} else {
				var response = {

					"error": "Parameter 'from' should be of the format dd-mm-yyyy",
					"status": "400"

				}
				context.sendResponse(response, "application/json");


			}
		} else {

			var response = {

				"error": "Parameter 'from' missing or is blank",
				"status": "422"

			}
			context.sendResponse(response, "application/json");

		}
	} else if (headers.authorization == undefined) {

		var response = {

			"error": "authorization parameter missing or value missing",
			"status": "400"

		}
		context.sendResponse(response);

	}



});