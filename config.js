var production = {
    BOT: {
        apikey: "19835fff62c7469bc69afa708c849c74",
        name: "moslbot",
        botkey:"b4ee9f77-adb8-471d-ab73-a2cf995ab768"
    },

    EMAILIDS: "rupali@motilaloswal.com,shaunak.nadig@gupshup.io,ajay.handoo@motilaloswal.com,prem@motilaloswal.com,premmehta79@gmail.com,nirav.patel@gupshup.io"
};

var development = {
    BOT: {
        apikey: "19835fff62c7469bc69afa708c849c74",
        name: "moslbot",
        botkey:"b4ee9f77-adb8-471d-ab73-a2cf995ab768"
    },

    EMAILIDS: "shravan.shan@gmail.com"
};

var Env_Config = production;

var constants = {

    AUTH_KEY: "81cee6b3-f6fa-4e61-86ae-991b36c59954",

    SETCREDS_SUCCESS: {
        "status": "SUCCESS",
        "message": "SUCCESSFULLY STORED CREDENTIALS OF __sessionid on " + Env_Config.BOT.name
    },

    SETCREDS_FAIL: {
        "status": "error",
        "message": "Failed to store credentials on " + Env_Config.BOT.name
    },

    DELCREDS_SUCCESS: {
		"status": "SUCCESS",
		"message": "SUCCESSFULLY DELETED CREDENTIALS OF __sessionid on " + Env_Config.BOT.name
	},

	DELCREDS_FAIL: {
		"status": "error",
		"message": "Failed to delete credentials on " + Env_Config.BOT.name
	},

	SENDMAIL_FAIL: {
		"status": "error",
		"message": "Invalid request headers"
	},

	MAIL_USERID: "2000700688",

	MAIL_PASSWORD: "ZCWcXb",

    MENULIST_API: "http://mofsl.co/api/GetMenuList",

    CLIENTCODE_API: "http://mofsl.co/api/Two/GetClientCodeBasedOnPan",

    TEXT: {
        login: "Your session is invalid! Please login",

        welcomeCategoryOne: "Hi !! I am GENIE." +
            "<br>Did you know that you can find out reports, details of your account and much more anywhere, anytime on Genie with just few words!" +
            "<br>Try one of the following selections." +
            "<br>Type /Select required option and I'll look it up for you." +
            "<br>For main menu Click/Type 'Home'",

        welcomeCategoryTwo: "Hi !! I am Alfred."+
			"<br>I can help you find the PMS reports anywhere, anytime. All you need to do is just type 3 words of the report that you want to view or click from the options available below and I will find it for you.",

        select: "Please select from below options",

        clientCode: "Please enter your client's code",

        dis_slipno: "Please enter your Slip Number",

        mf_portfoliono: "Please enter the folio number",

        customPeriod: "Please enter the period in the given format: dd/mm/yyyy to dd/mm/yyyy." +
            "<br>Please note, the period should be lesser than 3 months and within a particular financial year",

        invalidDate: "This doesn't seem like a valid date. <br>Please enter the period in the given format: dd/mm/yyyy to dd/mm/yyyy",

		invalidDateFy: "Please enter date within a particular financial year in the given format: dd/mm/yyyy to dd/mm/yyyy",
		
		customSaudaPeriod: "Enter Period in format -DD/MM/YYYY to DD/MM/YYYY (Disclaimer - data will be displayed for 90 days"+ 
		" at a time and is available for current financial year)",

		invalidDateSauda: "This doesn't seem like a valid date. <br>Please enter the period in the given format: dd/mm/yyyy to dd/mm/yyyy <br>Period range should be 90 days",

        invalidInput:"Sorry,that was an invalid input.",

        categoryTwoPeriod: "please enter the period in the given format: dd/mm/yyyy to dd/mm/yyyy",
        invalidDateHolding: "This doesn't seem like a valid date. <br>Please enter the period in the given format: dd/mm/yyyy",
        dp_note: "NOTE:Report is with a lag of 7 Days from current date<br>",
    },

    BUTTONS: {

        see_more: "{\"type\": \"load_more\",\"message\": \"more\",\"title\": \"See More\"}",

        ledger: "{\"type\":\"survey\",\"question\":\"_invalid <br>Please select from below options\",\"msgid\":\"ledger\",\"options\":[\"All Segment\",\"Equity & CD\",\"Commodity\",\"Go back\"]}",

        ledger_without_go_back: "{\"type\":\"survey\",\"question\":\"_invalid <br>Please select from below options\",\"msgid\":\"ledger\",\"options\":[\"All Segment\",\"Equity & CD\",\"Commodity\"]}",

        ledgersub: "{\"type\":\"survey\",\"question\":\"_invalid <br>Please select from below options\",\"msgid\":\"ledgersub\",\"options\":[\"3 months\",\"6 months\",\"Current FY\",\"Previous FY\",\"Enter Period\",\"Go back\"]}",

        ledgersub_without_go_back: "{\"type\":\"survey\",\"question\":\"_invalid <br>Please select from below options\",\"msgid\":\"ledgersub\",\"options\":[\"3 months\",\"6 months\",\"Current FY\",\"Previous FY\"]}",

        dp: "{\"type\":\"survey\",\"question\":\"_invalid <br>Please select from below options\",\"msgid\":\"dp\",\"options\":[\"Holding Statement\",\"Transaction Statement\",\"Go back\"]}",

        dpDefaultOutput: "{\"type\":\"survey\",\"question\":\"_invalid\",\"msgid\":\"dpsub\",\"options\":[{\"type\":\"home_link\",\"message\":\"home\",\"title\":\"Home\"}]}",

        dpsub: "{\"type\":\"survey\",\"question\":\"_invalid <br>Please select from below options\",\"msgid\":\"dpsub\",\"options\":[\"6 months\",\"Current FY\",\"Previous FY\",\"Enter Period\",\"Go back\"]}",

		pnlsub: "{\"type\":\"survey\",\"question\":\"_invalid <br>Please select from below options\",\"msgid\":\"pnlsub\",\"options\":[\"Current FY\",\"Previous FY\",\"Go back\"]}",
		
		saudasub: "{\"type\":\"survey\",\"question\":\"_invalid <br>Please select from below options\",\"msgid\":\"saudasub\",\"options\":[\"Previous FY\",\"Enter period\"]}",

        marginshortage: "{\"type\":\"survey\",\"question\":\"_invalid <br>Please select from below options\",\"msgid\":\"marginshortage\",\"options\":[\"FO\",\"Currency\",\"Commodity\",\"Go back\"]}",

        formatchecklist: "{\"type\":\"survey\",\"question\":\"Please select from below options\",\"msgid\":\"formatchecklist\",\"options\":[\"Modification forms\",\"Form and formats\",\"Process Note & Check List\",\"Go back\"]}",

        out: "{\"type\":\"survey\",\"question\":\"_out <br>Please select type of output\",\"msgid\":\"out\",\"options\":[\"View\",\"Email\",\"Go back\"]}",

        out_without_go_back: "{\"type\":\"survey\",\"question\":\"_out <br>Please select type of output\",\"msgid\":\"out\",\"options\":[\"View\",\"Email\"]}",
    
    	orderstatus: "{\"type\":\"survey\",\"question\":\"Please select from below options\",\"msgid\":\"orderstatus\",\"options\":[\"Lump Sum\",\"SIP\",\"XSIP\",\"ISIP\",\"Mandate\"]}",
    	
    	mspsub: "{\"type\":\"survey\",\"question\":\"_invalid <br>Please select from below options\",\"msgid\":\"mspsub\",\"options\":[\"Current FY\",\"Previous FY\",\"Go back\"]}",
    	
    	hpsResp: "{\"type\":\"survey\",\"question\":\"_apiresp <br><br>Click on Home for main menu\",\"msgid\":\"hpsresp\",\"options\":[\"Enter packet number\",{\"type\":\"home_link\",\"message\":\"home\",\"title\":\"Home\"}]}",
    
        dpsubCategoryTwo: "{\"type\":\"survey\",\"question\":\"_invalid <br>Please select from below options\",\"msgid\":\"dpsub\",\"options\":[\"3 months\",\"6 months\",\"Current FY\",\"Previous FY\",\"Enter Period\",\"Go back\"]}",
    	
    },

    RESPONSES: {
        apiresp: "{\"type\":\"survey\",\"question\":\"_apiresp <br><br>Click on Home for main menu\",\"msgid\":\"apiresp\",\"options\":[{\"type\":\"home_link\",\"message\":\"home\",\"title\":\"Home\"}]}",

		apiRespNonClient: "{\"type\":\"survey\",\"question\":\"_apiresp <br><br>Click on Home for main menu\",\"msgid\":\"apiRespNonClient\",\"options\":[{\"type\":\"home_link\",\"message\":\"home\",\"title\":\"Home\"},\"Enter another client code\"]}",
		
		last3contractnote: "{\"type\":\"survey\",\"question\":\"Click on link for last 3 Contract Notes <br> __response <br>To get another contract note please enter the period in the given format. Period range should be 30 days: dd/mm/yyyy to dd/mm/yyyy\",\"msgid\":\"apiresp\",\"options\":[{\"type\":\"home_link\",\"message\":\"home\",\"title\":\"Home\"}]}",
		
		ERROR: "Some error occurred"
    },

    FUNCTIONS: {
		"myDetails": {
			"endpoint": "ClientDetails"
		},
		"ledgerreport": {
			"endpoint": "LedgerBalanceSummary"
		},
		"ledger": {
			"endpoint": "LedgerStatement"
		},
		"dp": {
			"endpoint": "DPStatement"
		},
		"stt": {
			"endpoint": "STTCertificate"
		},
		"cn": {
			"endpoint": "ContractNote"
		},
		"cmr": {
			"endpoint": "CMRReport"
		},
		"margin": {
			"endpoint": "MarginShortage"
		},
		"ipo": {
			"endpoint": "ActiveIPO"
		},
		"accountclosure": {
			"endpoint": "AccountClosure"
		},
		"disdrfstatus": {
			"endpoint": "DPRequestStatus"
		},
		"mf": {
			"endpoint": "MutualFundStatement"
		},
		"profitandloss": {
			"endpoint": "CapitalGainLoss"
		},
		"dashboard": {
			"endpoint": "ClientView"
		},
		"fundpayoutstatus": {
			"endpoint": "CordysFundPayoutStatus"
		},
		"fundtransferstatus": {
			"endpoint": "FundsTransfer"
        },
        "formatchecklist": {
		    "endpoint": "Format_Check List"
		},
		"sauda": {
			"endpoint": "SaudaDetails"
		},
		"orderstatus":{
			"endpoint": "MF_ORDERSTATUS"
		},
		"disbook": {
		    "endpoint": "DisBookRequest"
		},
		"marginshortagepenalty": {
			"endpoint": "MarginShortagePenalty"
		},
		"hps": {
		    "endpoint": "Hits"
		},
		"quicklinks":{
			"endpoint": "QuickLinks"
		},
		"accountstatus": {
			"endpoint": "AccountObjectionStatus"
		},
		"myDetailsTwo": {
			"endpoint": "ClientDetail"
		},
		"cmrReportTwo": {
			"endpoint": "CMRReport"
		},
		"accountperformancereport": {
			"endpoint": "AccountPerformanceReport"
		},
		"corporateactionreport": {
			"endpoint": "CorporateActionReport"
		},
		"debitnotes": {
			"endpoint": "DebitNotes"
		},
		"capitalmovement": {
			"endpoint": "CapitalMovement"
		},
		"dividendledger": {
			"endpoint": "Dividendledger"
		},
		"incomeandexpensesstatement": {
			"endpoint": "IncomeandExpensesStatement"
		},
		"account": {
			"endpoint": "DownloadDocument",
		},
		"holdingStatement":{
		   "endpoint":"DpTransaction"
			
		},
		"transactionStatement":{
			"endpoint":"TransactionStatement"
		}
	 }
};

module.exports.ENV_CONFIG = Env_Config;
module.exports.CONSTANTS = constants;