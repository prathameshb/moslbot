var botScriptExecutor = require('bot-script').executor;
var scr_config = require('./scr_config.json');
const botRouter = require('gupshup-utils').BotRoutes();
const CONFIG = require('./config.js');
const Api = require('./api/api.js');
const BotApi = require('./api/botapi.js');
const Utils = require('./utils/utils.js');
const BotUtils = require('./utils/botutils.js');

function MessageHandler(context, event) {

    var msg = event.message.toLowerCase();
    
    var roomleveldata = context.simpledb.roomleveldata;
    if (msg == "clear" || msg=="home") {
    	var sessionid = roomleveldata.sessionid;
    	var token = roomleveldata.token;
    	var role = roomleveldata.role;
    	var category = roomleveldata.category;
    	var baseurl = roomleveldata.baseurl;
    	var labels = roomleveldata.labels;
    	var userid = roomleveldata.userid;
    	
        context.simpledb.roomleveldata = {};
        roomleveldata = context.simpledb.roomleveldata;
        
        roomleveldata.sessionid = sessionid;
        roomleveldata.token = token;
        roomleveldata.role = role;
        roomleveldata.category = category;
        roomleveldata.baseurl = baseurl;
        roomleveldata.labels = labels;
        roomleveldata.userid = userid;
        console.log("roomleveldata", roomleveldata);
    }
    
    console.log("CONTEXT ID",event.contextobj.contextid);
    if (!roomleveldata.sessionid) {
        return context.sendResponse(CONFIG.CONSTANTS.TEXT.login);
    }
    
    if(event.messageobj.refmsgid =="apiRespNonClient"){
        event.messageobj.hasrefmsgid=true;
    }
    event.messageobj.refmsgid = "";
    ScriptHandler(context, event);
}

function EventHandler(context, event) {
    MessageHandler(context, event);
}


function ScriptHandler(context, event) {
    var options = Object.assign({}, scr_config);
    options.data = {};
    options.current_dir = __dirname;
    options.nlp_threshold=0.3;
    // You can add any start point by just mentioning the <script_file_name>.<section_name>
    // options.start_section = "default.main";
    options.default_message = "{\"type\":\"survey\",\"question\":\"Sorry, I did not understand. Please ask again or click on Home for main menu.\",\"msgid\":\"default_message\",\"options\":[{\"type\":\"home_link\",\"message\":\"home\",\"title\":\"Home\"}]}";
    options.debug=1;
    options.escapeKeywords = ["hi","hello","menu","home"];
    options.success = function(opm) {
    	console.log("OPM---->",opm);
    	let lastMessage = "";
    	while((lastMessage === "") || (typeof lastMessage === "undefined")){
    		lastMessage = opm.pop();
    	}
    	
    	if(lastMessage==options.default_message){
    		
    	  console.log("LAST MESSAGE",lastMessage);	
    	  
    	  BotUtils.storeFailureResponses(context,event,function(res){
        	
           if(res){
            
              console.log("IN RES",res);
              
              context.sendResponse(options.default_message);  	
           }	
        	
        });
    		
    	}
    	else if(typeof lastMessage === "object"){
    		context.simpledb.roomleveldata["prev_msg"]=lastMessage;
    		context.sendResponse(JSON.stringify(lastMessage));
    	}else if(typeof lastMessage === "string"){
    		context.simpledb.roomleveldata["prev_msg"]=lastMessage;
    		context.sendResponse(lastMessage);	
    	}else{
    		context.simpledb.roomleveldata["prev_msg"]=lastMessage;
    		context.sendResponse(lastMessage);
    	}
        
    };
    options.error = function(err) {
        console.log(err);
        console.log(err.stack)
        console.log("Error Handler Called");
        context.sendResponse(options.default_message);
    };
    botScriptExecutor.execute(options, event, context);
}

function HttpResponseHandler(context, event) {
    if (event.geturl === "http://ip-api.com/json")
        context.sendResponse('This is response from http \n' + JSON.stringify(event.getresp, null, '\t'));
}

function DbGetHandler(context, event) {
    context.sendResponse("testdbput keyword was last sent by:" + JSON.stringify(event.dbval));
}

function DbPutHandler(context, event) {
    context.sendResponse("testdbput keyword was last sent by:" + JSON.stringify(event.dbval));
}

function HttpEndpointHandler(context, event) {
    context.sendResponse('This is response from http \n' + JSON.stringify(event, null, '\t'));
}

function LocationHandler(context, event) {
    context.sendResponse("Got location");
};

exports.onMessage = MessageHandler;
exports.onEvent = EventHandler;
exports.onHttpResponse = HttpResponseHandler;
exports.onDbGet = DbGetHandler;
exports.onDbPut = DbPutHandler;
if (typeof LocationHandler == 'function') {
    exports.onLocation = LocationHandler;
}
// if (typeof HttpEndpointHandler == 'function') { exports.onHttpEndpoint = HttpEndpointHandler; }
exports.onHttpEndpoint = botRouter.HttpEndpointHandler;

exports.Api = Api;
exports.BotApi = BotApi;
exports.CONFIG = CONFIG;
exports.Utils = Utils;
exports.BotUtils = BotUtils;