module.exports.ledgerReport = {
    ledgerReportApiCall:ledgerReportApiCall
};

function ledgerReportApiCall(options, event, context, callback) {
    console.log("ledgerReportApiCall called"); //print ledger report response
    const Api = require('../../index.js').Api;
	Api.ledgerReport(context, event, function(response){//ledger report api call
        var data = {
            endpoint: "Ledger Report",
            segment: "NA",
            period: "NA"
        };
        const BotUtils = require('../../index.js').BotUtils;
        BotUtils.apiResponseHandler(context, response, data, function(messageToUser) {//log data and print response
        	var survey = JSON.parse(messageToUser);
        	survey.options.push("Ledger Statement");
            options.data.ledgerReportResponse = JSON.stringify(survey);
            console.log("next_state", options.next_state);
			callback(options, event, context);
        });
	});
}

module.exports.sttCertificate = {
    sttApiCall:sttApiCall
};

function sttApiCall(options, event, context, callback) {
    console.log("sttApiCall called"); //print stt certificate response
    const Api = require('../../index.js').Api;
	Api.sttCertificate(context, event, function(response){//stt certificate api call
        var data = {
            endpoint: "STT Certificate",
            segment: "NA",
            period: "NA"
        };
        const BotUtils = require('../../index.js').BotUtils;
        BotUtils.apiResponseHandler(context, response, data, function(messageToUser) {//log data and print response
            options.data.sttResponse = messageToUser;
            console.log("next_state", options.next_state);
			callback(options, event, context);
        });
	});
}

module.exports.clientDetails = {
    clientDetailsApiCall:clientDetailsApiCall
};

function clientDetailsApiCall(options, event, context, callback) {
    console.log("clientDetailsApiCall called"); //print client details response
    const Api = require('../../index.js').Api;
	Api.clientDetails(context, event, function(response){//client details api call
        var data = {
            endpoint: "Client Details",
            segment: "NA",
            period: "NA"
        };
        response = response.replace(/;/g,"<br>");
        const BotUtils = require('../../index.js').BotUtils;
        BotUtils.apiResponseHandler(context, response, data, function(messageToUser) {//log data and print response
            options.data.clientDetailsResponse = messageToUser;
            console.log("next_state", options.next_state);
			callback(options, event, context);
        });
	});
}

module.exports.cmrReport = {
    cmrReportApiCall:cmrReportApiCall
};

function cmrReportApiCall(options, event, context, callback) {
    console.log("cmrReportApiCall called"); //print cmr response response
    const Api = require('../../index.js').Api;
	Api.cmrReport(context, event, function(response){//cmr report api call
        var data = {
            endpoint: "CMR Report",
            segment: "NA",
            period: "NA"
        };
        const BotUtils = require('../../index.js').BotUtils;
        BotUtils.apiResponseHandler(context, response, data, function(messageToUser) {//log data and print response
            options.data.cmrResponse = messageToUser;
            console.log("next_state", options.next_state);
			callback(options, event, context);
        });
	});
}

module.exports.marginShortage = {
    marginShortageApiCall:marginShortageApiCall
};

function marginShortageApiCall(options, event, context, callback) {
    console.log("marginShortageApiCall called"); //print margin shortage response response
    const Api = require('../../index.js').Api;
	Api.marginShortage(context, event, function(response){//margin shortage api call
        var data = {
            endpoint: "Margin Shortage",
            segment: "NA",
            period: "NA"
        };
        const BotUtils = require('../../index.js').BotUtils;
        BotUtils.apiResponseHandler(context, response, data, function(messageToUser) {//log data and print response
            options.data.marginResponse = messageToUser;
            console.log("next_state", options.next_state);
			callback(options, event, context);
        });
	});
}

module.exports.activeIpo = {
    activeIpoApiCall:activeIpoApiCall
};

function activeIpoApiCall(options, event, context, callback) {
    console.log("activeIpoApiCall called"); //print active ipo response response
    const Api = require('../../index.js').Api;
	Api.activeIpo(context, event, function(response){//active ipo api call
        var data = {
            endpoint: "Active IPO",
            segment: "NA",
            period: "NA"
        };
        const BotUtils = require('../../index.js').BotUtils;
        BotUtils.apiResponseHandler(context, response, data, function(messageToUser) {//log data and print response
            options.data.activeIpoResponse = messageToUser;
            console.log("next_state", options.next_state);
			callback(options, event, context);
        }, false);
	});
}

module.exports.accountClosure = {
    accountClosureApiCall:accountClosureApiCall
};

function accountClosureApiCall(options, event, context, callback) {
    console.log("accountClosureApiCall called"); //print account closure response response
    const Api = require('../../index.js').Api;
	Api.accountClosure(context, event, function(response){//account closure api call
        var data = {
            endpoint: "Account Closure",
            segment: "NA",
            period: "NA"
        };
        const BotUtils = require('../../index.js').BotUtils;
        BotUtils.apiResponseHandler(context, response, data, function(messageToUser) {//log data and print response
            options.data.accountClosureResponse = messageToUser;
            console.log("next_state", options.next_state);
			callback(options, event, context);
        });
	});
}

module.exports.dashboard = {
    dashboardClosureApiCall:dashboardClosureApiCall
};

function dashboardClosureApiCall(options, event, context, callback) {
    console.log("dashboardClosureApiCall called"); //print dashboard response response
    const Api = require('../../index.js').Api;
	Api.dashboard(context, event, function(response){//dashboard api call
        var data = {
            endpoint: "Client Dashboard",
            segment: "NA",
            period: "NA"
        };
        const BotUtils = require('../../index.js').BotUtils;
        BotUtils.apiResponseHandler(context, response, data, function(messageToUser) {//log data and print response
            options.data.dashboardClosureResponse = messageToUser;
            console.log("next_state", options.next_state);
			callback(options, event, context);
        });
	});
}

module.exports.fundPayoutStatus = {
    fundPayoutStatusApiCall:fundPayoutStatusApiCall
};

function fundPayoutStatusApiCall(options, event, context, callback) {
    console.log("fundPayoutStatusApiCall called"); //print fund payout status response response
    const Api = require('../../index.js').Api;
	Api.fundPayoutStatus(context, event, function(response){//fund payout status api call
        var data = {
            endpoint: "Fund Payout Status",
            segment: "NA",
            period: "NA"
        };
        const BotUtils = require('../../index.js').BotUtils;
        BotUtils.apiResponseHandler(context, response, data, function(messageToUser) {//log data and print response
            options.data.fundPayoutStatusResponse = messageToUser;
            console.log("next_state", options.next_state);
			callback(options, event, context);
        });
	});
}

module.exports.fundTransferStatus = {
    fundTransferStatusApiCall:fundTransferStatusApiCall
};

function fundTransferStatusApiCall(options, event, context, callback) {
    console.log("fundTransferStatusApiCall called"); //print fund payout status response response
    const Api = require('../../index.js').Api;
	Api.fundTransferStatus(context, event, function(response){//fund payout status api call
        var data = {
            endpoint: "Fund Transfer Status",
            segment: "NA",
            period: "NA"
        };
        const BotUtils = require('../../index.js').BotUtils;
        BotUtils.apiResponseHandler(context, response, data, function(messageToUser) {//log data and print response
            options.data.fundTransferStatusResponse = messageToUser;
            console.log("next_state", options.next_state);
			callback(options, event, context);
        });
	});
}

module.exports.quickLinks = {
    quickLinksApiCall:quickLinksApiCall
};

function quickLinksApiCall(options, event, context, callback) {
    console.log("quickLinksApiCall called"); //print quick links response
    var roomleveldata = context.simpledb.roomleveldata;
    roomleveldata.orderType = "1";
    const Api = require('../../index.js').Api;
    Api.quickLinks(context, event, function(response){//quick links api call
        var data = {
            endpoint: "Quick Links",
            segment: roomleveldata.orderType,
            period: "NA"
        };
        const BotUtils = require('../../index.js').BotUtils;
        BotUtils.apiResponseHandler(context, response, data, function(messageToUser) {//log data and print response
            options.data.quickLinksResponse = messageToUser;
            console.log("next_state", options.next_state);
            callback(options, event, context);
        }, false);
    });
}

module.exports.accountStatus = {
    accountStatusApiCall:accountStatusApiCall
};

function accountStatusApiCall(options, event, context, callback) {
    console.log("accountStatusApiCall called"); //print account status response
    var roomleveldata = context.simpledb.roomleveldata;
    const Api = require('../../index.js').Api;
    Api.accountstatusoutput(context, event, function(response){//account status api call
        var data = {
            endpoint: "Account Status",
            segment: "NA",
            period: "NA"
        };
        const BotUtils = require('../../index.js').BotUtils;
        BotUtils.apiResponseHandler(context, response, data, function(messageToUser) {//log data and print response
            options.data.accountStatusResponse = messageToUser;
            console.log("next_state", options.next_state);
            callback(options, event, context);
        });
    });
}

module.exports.clientCode = {
    setClientCodeText: setClientCodeText,
    setUserEnteredClientCode: setUserEnteredClientCode
};

function setClientCodeText(options, event, context, callback){
    console.log("setClientCodeText called");//ask user for client code
    const CONFIG = require('../../index.js').CONFIG;
    options.data.askClientCode = CONFIG.CONSTANTS.TEXT.clientCode;
    console.log("next_state", options.next_state);
    callback(options, event, context);
}

function setUserEnteredClientCode(options, event, context, callback){
    console.log("setUserEnteredClientCode called");//set user entered client code
    var roomleveldata = context.simpledb.roomleveldata;
    roomleveldata.oldclientid = roomleveldata.clientid;
    var text = event.message;
    roomleveldata.clientid = text;
    const Api = require('../../index.js').Api;
    Api.validateClientDetails(context, event, function(isValidClientId){
      	 if(isValidClientId){
      	 	options.next_state = "validClientId";
      	 } else {
      		options.next_state = "invalidClientId";	
      		// options.data.
      	 }
      	 console.log("next_state", options.next_state);
    	 callback(options, event, context);
    });
}
module.exports.newaccountopening={
newaccountApiCall:newaccountApiCall	
	
}
function newaccountApiCall(options, event, context, callback) {
    console.log("newaccountApiCall called"); //print quick links response
    var roomleveldata = context.simpledb.roomleveldata;
  
    const Api = require('../../index.js').Api;
    Api.accountoutput(context, event, function(response){//quick links api call
	    console.log("api response : ", response);
	    try {
	    	/* code */
	    	var data = {
	            endpoint: "New Account Opening",
	            segment: "NA",
	            period: "NA"
	        };
	        const BotUtils = require('../../index.js').BotUtils;
	        BotUtils.apiResponseHandler(context, response, data, function(messageToUser) {//log data and print response
	            options.data.apiresp = messageToUser;
	            console.log("next_state", options.next_state);
	            callback(options, event, context);
	        }, false);
	    } catch (e) {
	    	console.log(e);
	    }
        
    });
}

module.exports.ApiCall = {
	apiCall:apiCall	
};

function apiCall(options, event, context, callback) {
	console.log("apiCall called");
	var text = event.message;
	var roomleveldata = context.simpledb.roomleveldata;
	console.log(roomleveldata.clientid, "\n", text, "\n", roomleveldata.params);
	roomleveldata.params = roomleveldata.params.replace(roomleveldata.oldclientid, text);
	roomleveldata.clientid = text;
	const BotUtils = require('../../index.js').BotUtils;
	BotUtils.httpPost(context, roomleveldata.url, roomleveldata.params, roomleveldata.headers, function(response){
		var data = roomleveldata.logData;
        BotUtils.apiResponseHandler(context, response, data, function(messageToUser) {//log data and print response
            options.data.apiResponse = messageToUser;
            console.log("next_state", options.next_state);
            callback(options, event, context);
        });		
	});
}