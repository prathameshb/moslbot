module.exports.lastThreeContractNote = {
    lastThreeContractNote:lastThreeContractNote,
    userEntersPeriod:userEntersPeriod,
    resetMenuOffset2:resetMenuOffset2
};

function lastThreeContractNote(options, event, context, callback) {
    console.log("lastThreeContractNote called"); //print last 3 contract note and ask period
    const Api = require('../../index.js').Api;
    Api.lastThreeContractNote(context, event, function(response){
        try {
            response = JSON.parse(response);
        } catch (error) {
            //
        }
        const CONFIG = require('../../index.js').CONFIG;
        options.data.lastThreeContractNoteAndPeriodQuestion = CONFIG.CONSTANTS.RESPONSES.last3contractnote.replace("__response",response.message);
        console.log("next_state", options.next_state);
        callback(options, event, context);
    });
}

function userEntersPeriod(options, event, context, callback) {
    console.log("userEntersPeriod called");
    var roomleveldata = context.simpledb.roomleveldata;
    var msg = event.message.toLowerCase();
    const CONFIG = require('../../index.js').CONFIG;
    // if(msg=="hi" || msg=="clear" || msg.includes("menu") || msg=="home"){
    // 	options.next_state="smalltalk"
    // 	callback(options,event,context);
    	
    // }
    
    if (msg.includes("to")) {
        msg = msg.replace(/ /g, "");
        var x = msg.split("to");
        var d1 = x[0];
        var d2 = x[1];
        const Utils = require('../../index.js').Utils;
        var t1 = Utils.parseDate(d1);
        var t2 = Utils.parseDate(d2);
        if (t1 && t2) {//if dates are valid
            var customPeriod = Utils.customPeriod(context, event, d1, d2);
            var validation = Utils.validateDate(new Date(customPeriod.da1).getTime(), new Date(customPeriod.da2).getTime());
            if (validation) {//if date range is valid i.e. dates entered are before current date.
                if (parseInt(customPeriod.diffDays) <= 30) {//if days difference is less than 1 month
                    roomleveldata.startDate = customPeriod.da1;
                    roomleveldata.endDate = customPeriod.da2;
                    options.next_state = "validDate";
                    options.data.outputType = CONFIG.CONSTANTS.BUTTONS.out.replace(/_out/g, customPeriod.dateMsg);
                } else {
                    options.next_state = "invalidDate";
                    options.data.invalidDateMsg = CONFIG.CONSTANTS.TEXT.invalidDate;
                }
            } else {
            	options.next_state = "invalidDate";
        		options.data.invalidDateMsg = CONFIG.CONSTANTS.TEXT.invalidDate;
            }
        }
    } else {//if dates are invalid
    	options.next_state = "invalidDate";
        options.data.invalidDateMsg = CONFIG.CONSTANTS.TEXT.invalidDate;
    }

    console.log("next_state", options.next_state);
    callback(options, event, context);
}

module.exports.outputType = {
    view:view,
    email:email,
    resetMenuOffset2:resetMenuOffset2
};
function resetMenuOffset2(options, event, context, callback) {
	console.log("resetMenuOffset called");
	var roomleveldata = context.simpledb.roomleveldata;
	roomleveldata.menuOffset = 0; //reset menuOffset to show first 5 options.
	console.log("next_state", options.next_state);
	callback(options, event, context);
}

function view(options, event, context, callback){
	console.log("view called");
	var roomleveldata = context.simpledb.roomleveldata;
	roomleveldata.outputType = "Link";
    const Api = require('../../index.js').Api;
	Api.contractNote(context, event, function(response){//contract note api call
		contractNoteApiResponseCallback(context, response, function(messageToUser) {//message to user
			options.data.contractNoteResponse = messageToUser;
			console.log("next_state", options.next_state);
			callback(options, event, context);
		});
	});
}

function email(options, event, context, callback){
	console.log("email called");
	var roomleveldata = context.simpledb.roomleveldata;
	roomleveldata.outputType = "Email";
    const Api = require('../../index.js').Api;
	Api.contractNote(context, event, function(response){//contract note api call
		contractNoteApiResponseCallback(context, response, function(messageToUser) {//message to user
			options.data.contractNoteResponse = messageToUser;
			console.log("next_state", options.next_state);
			callback(options, event, context);
		});
	});
}

function contractNoteApiResponseCallback(context, response, callback) {
	var roomleveldata = context.simpledb.roomleveldata;
    var data = {
        endpoint: "Contract Note",
        segment: "NA",
        period: roomleveldata.startDate + "-" + roomleveldata.endDate
    }
    const BotUtils = require('../../index.js').BotUtils;
    BotUtils.apiResponseHandler(context, response, data, function(messageToUser) {//log data
        callback(messageToUser);
    });
}
function resetMenuOffset2(options, event, context, callback) {
	console.log("resetMenuOffset called");
	var roomleveldata = context.simpledb.roomleveldata;
	roomleveldata.menuOffset = 0; //reset menuOffset to show first 5 options.
	console.log("next_state", options.next_state);
	callback(options, event, context);
}