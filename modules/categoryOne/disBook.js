module.exports.disBookId = {
	getAllDpIds: getAllDpIds,
	getspecificDpId: getspecificDpId
};

// function get_slipno(options,event,context,callback){
// 	console.log("get_slipno for dis book request called");
// 	let roomData = context.simpledb.roomleveldata;
// 	let msg=event.message.toLowerCase();
// 	roomData["dpid"]=event.message;
//     const CONFIG = require('../../index.js').CONFIG;
//     const Api = require('../../index.js').Api;
// 	Api.disbookoutput(context, event, function(response){
// 		var data = {
//             endpoint: "DIS Book Request",
//             segment:roomData["dpid"] ,
//             period: "NA"
//         };
//         const BotUtils = require('../../index.js').BotUtils;
//         BotUtils.apiResponseHandler(context, response, data, function(messageToUser) {//log data and print response
//             options.data.api_resp = messageToUser;
//             options.next_state="api_resp";
//             console.log("next_state", options.next_state);
// 			callback(options, event, context);
//         });
// 	});
// }

function getAllDpIds(options, event, context, callback) {
	console.log(" getAllDpIds called");
	let roomData = context.simpledb.roomleveldata;
	roomData["dpid"] = "";
	const CONFIG = require('../../index.js').CONFIG;
	const Api = require('../../index.js').Api;
	Api.disbookoutput(context, event, function(response) {
		try {
			response = JSON.parse(response);
		} catch (error) {
			console.log(error);
		}

		var status = response.status;
		var message = response.message;
		console.log("log:status:" + status + " message:" + message);
		var responsemsg = response.message;
		var dpids = response.dpId;
		console.log("dpids---", dpids, response);
		if (dpids != undefined && dpids != "") {
			var dpidArray = dpids.split(",");
			if (dpidArray.length) {
				var surveryoptions = [{
					type: "home_link",
					message: "home",
					title: "Home"
				}];
				for (var index = 0; index < dpidArray.length; index++) {
					surveryoptions.push({
						type: "home_link",
						message: dpidArray[index],
						title: dpidArray[index]
					});
				}
				var survey = {};
				survey.type = "survey";
				survey.msgid = "disbooksub";
				survey.question = responsemsg + "<br>Click on Home for main menu";
				survey.options = surveryoptions;

			} else {
				var survey = {};
				survey.type = "survey";
				survey.msgid = "disbooksub";
				survey.question = responsemsg + "<br>Click on Home for main menu";
				survey.options = [{
					type: "home_link",
					message: "home",
					title: "Home"
				}];

			}
			options.data.all_dpid_survey = JSON.stringify(survey);
			callback(options, event, context);

		} else {


           
			var survey = {};
			survey.type = "survey";
			survey.msgid = "disbooksub";
			survey.question = responsemsg + "<br>Click on Home for main menu";
			survey.options = [{
				type: "home_link",
				message: "home",
				title: "Home"
			}];
			options.data.all_dpid_survey = JSON.stringify(survey);
			callback(options, event, context);
            
		}


	});

}

function getspecificDpId(options, event, context, callback) {
	console.log(" getspecificDpId called");
	let roomData = context.simpledb.roomleveldata;
	roomData["dpid"] = event.message;
	const CONFIG = require('../../index.js').CONFIG;
	const Api = require('../../index.js').Api;
	Api.disbookoutput(context, event, function(response) {
		// try {
		// 	response = JSON.parse(response);
		// } catch (error) {
		// 	console.log(error);
		// }
		// console.log("response in disbookoutput callback ", response);
		// var finalResp = CONFIG.CONSTANTS.RESPONSES.apiresp.replace("_apiresp", response.message);
		// console.log("FINAL RESP",finalResp);
		disBookApiResponseCallback(context, response, function(messageToUser) {
				// options.data.api_resp = finalResp;
				options.next_state = "printDpIdResponse";
				options.data.api_resp = messageToUser;
				console.log("messageToUser", messageToUser);
				callback(options, event, context);
			});
		
	});

	

}

function disBookApiResponseCallback(context, response, callback) {
	console.log("disBookApiResponseCallback called");
	var roomData = context.simpledb.roomleveldata;
	var data = {
		endpoint: "DIS Book Request",
		segment: roomData["dpid"],
		period: "NA"
	};
	const BotUtils = require('../../index.js').BotUtils;
	BotUtils.apiResponseHandler(context, response, data, function(messageToUser) { //log data
		callback(messageToUser);
	});
}