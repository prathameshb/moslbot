module.exports.slipNo = {
    askSlipNo:askSlipNo,
    userEntersSlipNo:userEntersSlipNo
};

function askSlipNo(options, event, context, callback){
    console.log("askSlipNo called");
    const CONFIG = require('../../index.js').CONFIG;
	options.data.slipNoQuestion = CONFIG.CONSTANTS.TEXT.dis_slipno;
	console.log("next_state", options.next_state);
	callback(options, event, context);
}

function userEntersSlipNo(options, event, context, callback){
    console.log("userEntersSlipNo called");
    var roomleveldata = context.simpledb.roomleveldata;
    roomleveldata.refNo = event.message;
    const Api = require('../../index.js').Api;
    Api.disdrfStatus(context, event, function(response){//DIS DRF Status api call
        var data = {
            endpoint: "DIS/DRF Status",
            segment: "NA",
            period: "NA"
        };
        const BotUtils = require('../../index.js').BotUtils;
        BotUtils.apiResponseHandler(context, response, data, function(messageToUser) {//log data and print response
            options.data.disdrfStatusResponse = messageToUser;
            options.next_state = "disdrfStatusResponse";
            console.log("next_state", options.next_state);
			callback(options, event, context);
        }, false);
	});
}