module.exports.dpMenu = {
	dpOptions:dpOptions,
	holdingStatement:holdingStatement,
	transactionStatement:transactionStatement,
	goBackToMainMenu:goBackToMainMenu
};

function dpOptions(options, event, context, callback) {
    console.log("dpOptions called"); //print dp options [Holding statement, Transaction statement]
    const CONFIG = require('../../index.js').CONFIG;
    options.data.dpOptions = CONFIG.CONSTANTS.BUTTONS.dp.replace(/_invalid/g, "DP Statement ");
    console.log("next_state", options.next_state);
    callback(options, event, context);
}

function holdingStatement(options, event, context, callback){
	console.log("holdingStatement called");//print default holding statement output
	var roomleveldata = context.simpledb.roomleveldata;
	roomleveldata.segment = "H";
	const Api = require('../../index.js').Api;
	Api.defaultDpStatement(context, event, function(response){
		try {
			response = JSON.parse(response);
		} catch (error) { 
			console.log(error);
		}
        const CONFIG = require('../../index.js').CONFIG;
        var data = {
            endpoint: "DP Statement",
            segment: getSegment(roomleveldata.segment),
            period: roomleveldata.startDate + "-" + roomleveldata.endDate,
            status: response.status,
            messageToUser: CONFIG.CONSTANTS.BUTTONS.dpDefaultOutput.replace(/_invalid/g, "DP Statement->Holding Statement <br>"+response.message)
        }
        const BotUtils = require('../../index.js').BotUtils;
        BotUtils.logData(context, data, function() {//log data
            options.data.defaultDpStatementOutput = data.messageToUser;
            console.log("next_state", options.next_state);
            callback(options, event, context);
        });
	});
}

function transactionStatement(options, event, context, callback){
	console.log("transactionStatement called");//print transaction statement date options [6 months, Previous FY, Current FY, Enter period]
	var roomleveldata = context.simpledb.roomleveldata;
	roomleveldata.segment = "T";
	const Api = require('../../index.js').Api;
	Api.defaultDpStatement(context, event, function(response){
		try {
			response = JSON.parse(response);
		} catch (error) { 
			console.log(error);
		}
		const CONFIG = require('../../index.js').CONFIG;
		options.data.defaultDpStatementOutput = CONFIG.CONSTANTS.BUTTONS.dpsub.replace(/_invalid/g, "DP Statement->Transaction Statement <br>"+response.message+"<br>To get another statements ");
		console.log("next_state", options.next_state);
		callback(options, event, context);
	});
}

function goBackToMainMenu(options, event, context, callback) {
    console.log("goBackToMainMenu called"); //go back to main menu
    var roomleveldata = context.simpledb.roomleveldata;
    roomleveldata.menuOffset = 0;
    console.log("next_state", options.next_state);
    callback(options, event, context);
}

module.exports.transactionStatementDateSelection = {
    sixMonths: sixMonths,
    previousFy: previousFy,
    currentFy: currentFy,
    enterPeriod: enterPeriod,
    userEntersPeriod: userEntersPeriod
};

function sixMonths(options, event, context, callback) {
    console.log("sixMonths called"); //print dp output type [View, Email]
    const Utils = require('../../index.js').Utils;
    var dateMsg = Utils.period(context, event, -183); //subtract 183 days from current date for last 3 months
    const CONFIG = require('../../index.js').CONFIG;
    options.data.transactionStatementOutputType = CONFIG.CONSTANTS.BUTTONS.out.replace(/_out/g, dateMsg);
    console.log("next_state", options.next_state);
    callback(options, event, context);
}

function previousFy(options, event, context, callback) {
    console.log("previousFy called");
    const Utils = require('../../index.js').Utils;
    var dateMsg = Utils.fyPeriod(context, event, "previous"); //previous FY date range
    const CONFIG = require('../../index.js').CONFIG;
    options.data.transactionStatementOutputType = CONFIG.CONSTANTS.BUTTONS.out.replace(/_out/g, dateMsg);
    console.log("next_state", options.next_state);
    callback(options, event, context);
}

function currentFy(options, event, context, callback) {
    console.log("currentFy called");
    const Utils = require('../../index.js').Utils;
    var dateMsg = Utils.fyPeriod(context, event, "current"); //current FY date range
    const CONFIG = require('../../index.js').CONFIG;
    options.data.transactionStatementOutputType = CONFIG.CONSTANTS.BUTTONS.out.replace(/_out/g, dateMsg);
    console.log("next_state", options.next_state);
    callback(options, event, context);
}

function enterPeriod(options, event, context, callback) {
    console.log("enterPeriod called");//print 'Enter custom period'
    const CONFIG = require('../../index.js').CONFIG;
    options.data.dpCustomPeriod = CONFIG.CONSTANTS.TEXT.customPeriod;
    console.log("next_state", options.next_state);
    callback(options, event, context);
}

function userEntersPeriod(options, event, context, callback) {
    console.log("userEntersPeriod called");
    var roomleveldata = context.simpledb.roomleveldata;
    var msg = event.message.toLowerCase();
    const CONFIG = require('../../index.js').CONFIG;
    if (msg.includes("to")) {
        msg = msg.replace(/ /g, "");
        var x = msg.split("to");
        var d1 = x[0];
        var d2 = x[1];
        const Utils = require('../../index.js').Utils;
        var t1 = Utils.parseDate(d1);
        var t2 = Utils.parseDate(d2);
        if (t1 && t2) {//if dates are valid
            var customPeriod = Utils.customPeriod(context, event, d1, d2);
            var validation = Utils.validateDate(new Date(customPeriod.da1).getTime(), new Date(customPeriod.da2).getTime());
            if (validation) {//if date range is valid i.e. dates entered are before current date.
                if (customPeriod.timeDiff > 0) {//if time difference in date range is positive
                    if (customPeriod.fyValidation) {//if financial year validation fails i.e. April 1 falls between start and end date then it is not valid date range
                        options.next_state = "invalidDate";
                        options.data.invalidDateMsg = CONFIG.CONSTANTS.TEXT.invalidDateFy;
                    } else {
                        if (customPeriod.diffDays >= 91) {//if days difference is more than 3 months
                            options.next_state = "reEnterPeriod";
                            options.data.defaultDpStatementOutput = CONFIG.CONSTANTS.BUTTONS.dpsub.replace(/_invalid/g, "For period >3 months, ");
                        } else {//all validations passed
                            roomleveldata.startDate = customPeriod.da1;
                            roomleveldata.endDate = customPeriod.da2;
                            options.next_state = "validDate";
                            options.data.validDateMsg = CONFIG.CONSTANTS.BUTTONS.out.replace(/_out/g, customPeriod.dateMsg);
                        }
                    }
                } else {
	            	options.next_state = "invalidDate";
	        		options.data.invalidDateMsg = CONFIG.CONSTANTS.TEXT.invalidDate;
	            }
            } else {
            	options.next_state = "invalidDate";
        		options.data.invalidDateMsg = CONFIG.CONSTANTS.TEXT.invalidDate;
            }
        }
    } else {//if dates are invalid
    	options.next_state = "invalidDate";
        options.data.invalidDateMsg = CONFIG.CONSTANTS.TEXT.invalidDate;
    }

    console.log("next_state", options.next_state);
    callback(options, event, context);
}

module.exports.transactionStatementOutputType = {
	goBackToTransactionStatementDateSelection:goBackToTransactionStatementDateSelection,
    view:view,
    email:email
};

function goBackToTransactionStatementDateSelection(options, event, context, callback) {
    console.log("goBackToTransactionStatementDateSelection called"); //print transaction statement date options [6 months, Previous FY, Current FY, Enter period]
	const CONFIG = require('../../index.js').CONFIG;
    options.data.defaultDpStatementOutput = CONFIG.CONSTANTS.BUTTONS.dpsub.replace(/_invalid/g, "DP Statement->Transaction Statement ");
    console.log("next_state", options.next_state);
    callback(options, event, context);
}

function view(options, event, context, callback){
	console.log("view called");
	var roomleveldata = context.simpledb.roomleveldata;
	roomleveldata.outputType = "Link";
    const Api = require('../../index.js').Api;
    Api.dpStatement(context, event, function(response){//dp statement api call
    	dpApiResponseCallback(context, response, function(messageToUser) {//message to user
			options.data.transactionStatementResponse = messageToUser;
			console.log("next_state", options.next_state);
			callback(options, event, context);
		});
    });
}

function email(options, event, context, callback){
	console.log("email called");
	var roomleveldata = context.simpledb.roomleveldata;
	roomleveldata.outputType = "Email";
    const Api = require('../../index.js').Api;
    Api.dpStatement(context, event, function(response){//dp statement api call
    	dpApiResponseCallback(context, response, function(messageToUser) {//message to user
			options.data.transactionStatementResponse = messageToUser;
			console.log("next_state", options.next_state);
			callback(options, event, context);
		});
    });
}

function getSegment(segment) {
    segment = segment.toLowerCase();
    if (segment == "h") {
        return "Holding statement";
    } else if (segment == "t") {
        return "Transaction statement";
    } else {
        return "NA";
    }
}

function dpApiResponseCallback(context, response, callback) {
	var roomleveldata = context.simpledb.roomleveldata;
    var data = {
        endpoint: "DP Statement",
        segment: getSegment(roomleveldata.segment),
        period: roomleveldata.startDate + "-" + roomleveldata.endDate
    }
    const BotUtils = require('../../index.js').BotUtils;
    BotUtils.apiResponseHandler(context, response, data, function(messageToUser) {//log data
        callback(messageToUser);
    });
}