module.exports.formType = {
    askFormType:askFormType,
    modificationForm:modificationForm,
    formAndFormats:formAndFormats,
    processNoteAndCheckList:processNoteAndCheckList,
    goBackToMainMenu: goBackToMainMenu
};

function askFormType(options, event, context, callback){
    console.log("askFormType called");
    const CONFIG = require('../../index.js').CONFIG;
	options.data.formType = CONFIG.CONSTANTS.BUTTONS.formatchecklist;
	console.log("next_state", options.next_state);
	callback(options, event, context);
}

function modificationForm(options, event, context, callback){
	console.log("modificationForm called");
	var roomleveldata = context.simpledb.roomleveldata;
	roomleveldata.documentType = "1";
    const Api = require('../../index.js').Api;
	Api.formatCheckList(context, event, function(response){//format check list api call
		formatCheckListApiResponseCallback(context, response, function(messageToUser) {//message to user
			options.data.formResponse = messageToUser;
			console.log("next_state", options.next_state);
			callback(options, event, context);
		});
	});
}

function formAndFormats(options, event, context, callback){
	console.log("formAndFormats called");
	var roomleveldata = context.simpledb.roomleveldata;
	roomleveldata.documentType = "2";
    const Api = require('../../index.js').Api;
	Api.formatCheckList(context, event, function(response){//format check list api call
		formatCheckListApiResponseCallback(context, response, function(messageToUser) {//message to user
			options.data.formResponse = messageToUser;
			console.log("next_state", options.next_state);
			callback(options, event, context);
		});
	});
}

function processNoteAndCheckList(options, event, context, callback){
	console.log("processNoteAndCheckList called");
	var roomleveldata = context.simpledb.roomleveldata;
	roomleveldata.documentType = "3";
    const Api = require('../../index.js').Api;
	Api.formatCheckList(context, event, function(response){//format check list api call
		formatCheckListApiResponseCallback(context, response, function(messageToUser) {//message to user
			options.data.formResponse = messageToUser;
			console.log("next_state", options.next_state);
			callback(options, event, context);
		});
	});
}

function goBackToMainMenu(options, event, context, callback) {
    console.log("goBackToMainMenu called"); //go back to main menu
    var roomleveldata = context.simpledb.roomleveldata;
    roomleveldata.menuOffset = 0;
    console.log("next_state", options.next_state);
    callback(options, event, context);
}

function formatCheckListApiResponseCallback(context, response, callback) {
	var roomleveldata = context.simpledb.roomleveldata;
    var data = {
        endpoint: "Format/Check List",
        segment: "NA",
        period: "NA"
    }
    const BotUtils = require('../../index.js').BotUtils;
    BotUtils.apiResponseHandler(context, response, data, function(messageToUser) {//log data
        callback(messageToUser);
    });
}