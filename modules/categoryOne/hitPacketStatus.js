module.exports.packetNo={
	
	getAllPackets:getAllPackets,
	getspecificPacket:getspecificPacket
}
function getAllPackets(options,event,context,callback){
	console.log("getAllPackets called");
	let roomData = context.simpledb.roomleveldata;
	let msg=event.message.toLowerCase();
    const CONFIG = require('../../index.js').CONFIG;
    const Api = require('../../index.js').Api;
    roomData["packetNo"]="";
	Api.hpsoutput(context, event, function(response){
		try {
			response = JSON.parse(response);
		} catch (error) { 
			console.log(error);
		}
		console.log("Response from getAllPackets",JSON.stringify(response));
		let packetNos=response.type;
		if(packetNos!=undefined && packetNos!=""){
			var packetNosArray = packetNos.split(",");
				if (packetNosArray.length && packetNos.length) {
					var home_link = {
						type: "home_link",
						message: "home",
						title: "Home"
					};
				
					var surveyoptions = [];
					for (var index = 0; index < packetNosArray.length; index++) {
	
						surveyoptions.push(packetNosArray[index]);
					}
					surveyoptions.push(home_link);
					var survey = {};
					survey.type = "survey";
					survey.msgid = "hpssub";
					survey.question = response.message + "<br>Click on Home for main menu";
					survey.options = surveyoptions;
					options.data.all_packet_survey=JSON.stringify(survey);
				} else {
					var survey = {};
					survey.type = "survey";
					survey.msgid = "hpssub";
					survey.question = response.message + "<br>Click on Home for main menu";
					survey.options = [{
						type: "home_link",
						message: "home",
						title: "Home"
					}];
					options.data.all_packet_survey=JSON.stringify(survey);
				}
				console.log("data", options.data);
				console.log("next_state", options.next_state);
				callback(options,event,context);
				
		}
		else{
			
			var survey = {};
					survey.type = "survey";
					survey.msgid = "hpssub";
					survey.question = response.message + "<br>Click on Home for main menu";
					survey.options = [{
						type: "home_link",
						message: "home",
						title: "Home"
					}];
					options.data.all_packet_survey=JSON.stringify(survey);
				
				console.log("data", options.data);
				console.log("next_state", options.next_state);
				callback(options,event,context);
			
		}
	});
}
function getspecificPacket(options,event,context,callback){
	console.log("getspecificPacket called");
	let roomData = context.simpledb.roomleveldata;
	let msg=event.message.toLowerCase();
    const CONFIG = require('../../index.js').CONFIG;
    const Api = require('../../index.js').Api;
    roomData["packetNo"]=msg;
	Api.hpsoutput(context, event, function(response){
		// try {
		// 	response = JSON.parse(response);
		// } catch (error) { 
		// 	console.log(error);
		// }

				var finalResp = CONFIG.CONSTANTS.RESPONSES.apiresp.replace("_apiresp", response.message);
				
			    console.log("FINAL RESP",finalResp);
				hitpacketApiResponseCallback(context,response,function(messageToUser){
						options.next_state = "printHitsPacketResponse";
						console.log("data", options.data);
						console.log("next_state", options.next_state);
						options.data.hitsPacketResponse=messageToUser;
						callback(options,event,context);
				});
			
	});
}

function hitpacketApiResponseCallback(context, response, callback) {
	var roomData = context.simpledb.roomleveldata;
   	var data = {
             endpoint: "Hit Packet Status Request",
             segment:roomData["packetNo"] ,
             period: "NA"
         };
    const BotUtils = require('../../index.js').BotUtils;
    BotUtils.apiResponseHandler(context, response, data, function(messageToUser) {//log data
        callback(messageToUser);
    });
}