module.exports.ledgerMenu = {
    ledgerOptions: ledgerOptions,
    allSegment: allSegment,
    equityAndCd: equityAndCd,
    commodity: commodity,
    goBackToMainMenu: goBackToMainMenu
};

function ledgerOptions(options, event, context, callback) {
    console.log("ledgerOptions called"); //print ledger options [All segment, Equity, Commodity]
    const CONFIG = require('../../index.js').CONFIG;
    options.data.ledgerOptions = CONFIG.CONSTANTS.BUTTONS.ledger.replace(/_invalid/g, "Ledger Statement ");
    console.log("next_state", options.next_state);
    callback(options, event, context);
}

function allSegment(options, event, context, callback) {
    console.log("allSegment called"); //print ledger date options [3 months, 6 months, Previous FY, Current FY, Enter period]
    var roomleveldata = context.simpledb.roomleveldata;
    roomleveldata.segment = "All";
    const CONFIG = require('../../index.js').CONFIG;
    options.data.ledgerDateOptions = CONFIG.CONSTANTS.BUTTONS.ledgersub.replace(/_invalid/g, "Ledger Statement->All Segment");
    console.log("next_state", options.next_state);
    callback(options, event, context);
}

function equityAndCd(options, event, context, callback) {
    console.log("equityAndCd called"); //print ledger date options [3 months, 6 months, Previous FY, Current FY, Enter period]
    var roomleveldata = context.simpledb.roomleveldata;
    roomleveldata.segment = "EQ";
    const CONFIG = require('../../index.js').CONFIG;
    options.data.ledgerDateOptions = CONFIG.CONSTANTS.BUTTONS.ledgersub.replace(/_invalid/g, "Ledger Statement->Equity & CD");
    console.log("next_state", options.next_state);
    callback(options, event, context);
}

function commodity(options, event, context, callback) {
    console.log("commodity called"); //print ledger date options [3 months, 6 months, Previous FY, Current FY, Enter period]
    var roomleveldata = context.simpledb.roomleveldata;
    roomleveldata.segment = "COM";
    const CONFIG = require('../../index.js').CONFIG;
    options.data.ledgerDateOptions = CONFIG.CONSTANTS.BUTTONS.ledgersub.replace(/_invalid/g, "Ledger Statement->Commodity");
    console.log("next_state", options.next_state);
    callback(options, event, context);
}

function goBackToMainMenu(options, event, context, callback) {
    console.log("goBackToMainMenu called"); //go back to main menu
    var roomleveldata = context.simpledb.roomleveldata;
    roomleveldata.menuOffset = 0;
    console.log("next_state", options.next_state);
    callback(options, event, context);
}

module.exports.ledgerDateSelection = {
    threeMonths: threeMonths,
    sixMonths: sixMonths,
    previousFy: previousFy,
    currentFy: currentFy,
    enterPeriod: enterPeriod,
    userEntersPeriod: userEntersPeriod
};

function threeMonths(options, event, context, callback) {
    console.log("threeMonths called"); //print ledger output type [View, Email]
    const Utils = require('../../index.js').Utils;
    var dateMsg = Utils.period(context, event, -91); //subtract 91 days from current date for last 3 months
    const CONFIG = require('../../index.js').CONFIG;
    options.data.ledgerOutputType = CONFIG.CONSTANTS.BUTTONS.out.replace(/_out/g, dateMsg);
    console.log("next_state", options.next_state);
    callback(options, event, context);
}

function sixMonths(options, event, context, callback) {
    console.log("sixMonths called"); //print ledger output type [View, Email]
    const Utils = require('../../index.js').Utils;
    var dateMsg = Utils.period(context, event, -183); //subtract 183 days from current date for last 3 months
    const CONFIG = require('../../index.js').CONFIG;
    options.data.ledgerOutputType = CONFIG.CONSTANTS.BUTTONS.out.replace(/_out/g, dateMsg);
    console.log("next_state", options.next_state);
    callback(options, event, context);
}

function previousFy(options, event, context, callback) {
    console.log("previousFy called");
    const Utils = require('../../index.js').Utils;
    var dateMsg = Utils.fyPeriod(context, event, "previous"); //previous FY date range
    const CONFIG = require('../../index.js').CONFIG;
    options.data.ledgerOutputType = CONFIG.CONSTANTS.BUTTONS.out.replace(/_out/g, dateMsg);
    console.log("next_state", options.next_state);
    callback(options, event, context);
}

function currentFy(options, event, context, callback) {
    console.log("currentFy called");
    const Utils = require('../../index.js').Utils;
    var dateMsg = Utils.fyPeriod(context, event, "current"); //current FY date range
    const CONFIG = require('../../index.js').CONFIG;
    options.data.ledgerOutputType = CONFIG.CONSTANTS.BUTTONS.out.replace(/_out/g, dateMsg);
    console.log("next_state", options.next_state);
    callback(options, event, context);
}

function enterPeriod(options, event, context, callback) {
    console.log("enterPeriod called");//print 'Enter custom period'
    const CONFIG = require('../../index.js').CONFIG;
    options.data.ledgerCustomPeriod = CONFIG.CONSTANTS.TEXT.customPeriod;
    console.log("next_state", options.next_state);
    callback(options, event, context);
}

function userEntersPeriod(options, event, context, callback) {
    console.log("userEntersPeriod called");
    var roomleveldata = context.simpledb.roomleveldata;
    var msg = event.message.toLowerCase();
    const CONFIG = require('../../index.js').CONFIG;
    if (msg.includes("to")) {
        msg = msg.replace(/ /g, "");
        var x = msg.split("to");
        var d1 = x[0];
        var d2 = x[1];
        const Utils = require('../../index.js').Utils;
        var t1 = Utils.parseDate(d1);
        var t2 = Utils.parseDate(d2);
        if (t1 && t2) {//if dates are valid
            var customPeriod = Utils.customPeriod(context, event, d1, d2);
            var validation = Utils.validateDate(new Date(customPeriod.da1).getTime(), new Date(customPeriod.da2).getTime());
            if (validation) {//if date range is valid i.e. dates entered are before current date.
                if (customPeriod.timeDiff > 0) {//if time difference in date range is positive
                    if (customPeriod.fyValidation) {//if financial year validation fails i.e. April 1 falls between start and end date then it is not valid date range
                        options.next_state = "invalidDate";
                        options.data.invalidDateMsg = CONFIG.CONSTANTS.TEXT.invalidDateFy;
                    } else {
                        if (customPeriod.diffDays >= 91) {//if days difference is more than 3 months
                            options.next_state = "reEnterPeriod";
                            options.data.ledgerDateOptions = CONFIG.CONSTANTS.BUTTONS.ledgersub.replace(/_invalid/g, "For period >3 months, ");
                        } else {//all validations passed
                            roomleveldata.startDate = customPeriod.da1;
                            roomleveldata.endDate = customPeriod.da2;
                            options.next_state = "validDate";
                            options.data.validDateMsg = CONFIG.CONSTANTS.BUTTONS.out.replace(/_out/g, customPeriod.dateMsg);
                        }
                    }
                } else {
	            	options.next_state = "invalidDate";
	        		options.data.invalidDateMsg = CONFIG.CONSTANTS.TEXT.invalidDate;
	            }
            } else {
            	options.next_state = "invalidDate";
        		options.data.invalidDateMsg = CONFIG.CONSTANTS.TEXT.invalidDate;
            }
        }
    } else {//if dates are invalid
    	options.next_state = "invalidDate";
        options.data.invalidDateMsg = CONFIG.CONSTANTS.TEXT.invalidDate;
    }

    console.log("next_state", options.next_state);
    callback(options, event, context);
}

module.exports.ledgerOutputType = {
    goBackToLedgerDateSelection: goBackToLedgerDateSelection,
    view:view,
    email:email
};

function goBackToLedgerDateSelection(options, event, context, callback) {
    console.log("goBackToLedgerDateSelection called"); //print ledger date options [3 months, 6 months, Previous FY, Current FY, Enter period]
    var roomleveldata = context.simpledb.roomleveldata;

    function ledgerSegmentText(segment) {
        switch (segment) {
            case "All":
                return "All Segment";
            case "EQ":
                return "EQ";
            case "COM":
                return "Commodity";
        }
    }
    const CONFIG = require('../../index.js').CONFIG;
    options.data.ledgerDateOptions = CONFIG.CONSTANTS.BUTTONS.ledgersub.replace(/_invalid/g, "Ledger Statement->" + ledgerSegmentText(roomleveldata.segment));
    console.log("next_state", options.next_state);
    callback(options, event, context);
}

function view(options, event, context, callback){
	console.log("view called");
	var roomleveldata = context.simpledb.roomleveldata;
	roomleveldata.outputType = "Link";
    const Api = require('../../index.js').Api;
	Api.ledgerStatement(context, event, function(response){//ledger statement api call
		ledgerApiResponseCallback(context, response, function(messageToUser) {//message to user
			options.data.ledgerResponse = messageToUser;
			console.log("next_state", options.next_state);
			callback(options, event, context);
		});
	});
}

function email(options, event, context, callback){
	console.log("email called");
	var roomleveldata = context.simpledb.roomleveldata;
	roomleveldata.outputType = "Email";
    const Api = require('../../index.js').Api;
	Api.ledgerStatement(context, event, function(response){//ledger statement api call
		ledgerApiResponseCallback(context, response, function(messageToUser) {//message to user
			options.data.ledgerResponse = messageToUser;
			console.log("next_state", options.next_state);
			callback(options, event, context);
		});
	});
}

function getSegment(segment) {
    segment = segment.toLowerCase();
    if (segment == "all") {
        return "All segment";
    } else if (segment == "eq") {
        return "Equity & CD";
    } else if (segment == "com") {
        return "Commodity";
    } else {
        return "NA";
    }
}

function ledgerApiResponseCallback(context, response, callback) {
	var roomleveldata = context.simpledb.roomleveldata;
    var data = {
        endpoint: "Ledger Statement",
        segment: getSegment(roomleveldata.segment),
        period: roomleveldata.startDate + "-" + roomleveldata.endDate
    }
    const BotUtils = require('../../index.js').BotUtils;
    BotUtils.apiResponseHandler(context, response, data, function(messageToUser) {//log data
        callback(messageToUser);
    });
}