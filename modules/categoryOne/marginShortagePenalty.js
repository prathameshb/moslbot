module.exports.menu = {
	setOptions:setOptions,
	currentFy:currentFy,
    previousFy:previousFy,
    goBackToMainMenu:goBackToMainMenu
};

function setOptions (options, event, context, callback) {
	var CONFIG = require('../../index.js').CONFIG;
	options.data.marginShortageOptions = CONFIG.CONSTANTS.BUTTONS.mspsub.replace(/_invalid/g, "Margin Shortage Penalty");
	callback(options, event, context);
}

function currentFy(options, event, context, callback) {
	console.log("currentFy called");
    const Utils = require('../../index.js').Utils;
    var dateMsg = Utils.fyPeriod(context, event, "current"); //current FY date range
    const CONFIG = require('../../index.js').CONFIG;
    options.data.outputTypeOptions = CONFIG.CONSTANTS.BUTTONS.out.replace(/_out/g, dateMsg);
    console.log("next_state", options.next_state);
    callback(options, event, context);
}

function previousFy(options, event, context, callback) {
    console.log("previousFy called");
    const Utils = require('../../index.js').Utils;
    var dateMsg = Utils.fyPeriod(context, event, "previous"); //previous FY date range
    const CONFIG = require('../../index.js').CONFIG;
    options.data.outputTypeOptions = CONFIG.CONSTANTS.BUTTONS.out.replace(/_out/g, dateMsg);
    console.log("next_state", options.next_state);
    callback(options, event, context);
}

function goBackToMainMenu(options, event, context, callback) {
    console.log("goBackToMainMenu called"); //go back to main menu
    var roomleveldata = context.simpledb.roomleveldata;
    roomleveldata.menuOffset = 0;
    console.log("next_state", options.next_state);
    callback(options, event, context);
}

module.exports.outputType = {
	view:view,
    email:email
};

function view(options, event, context, callback){
	console.log("view called");
	var roomleveldata = context.simpledb.roomleveldata;
	roomleveldata.outputType = "Link";
    const Api = require('../../index.js').Api;
    Api.marginShortagePenalty(context, event, function(response){//margin shortage penalty api call
    	mspApiResponseCallback(context, response, function(messageToUser) {//message to user
			options.data.marginShortageResponse = messageToUser;
			console.log("next_state", options.next_state);
			callback(options, event, context);
		});
    });
}

function email(options, event, context, callback){
    console.log("email called");
    var roomleveldata = context.simpledb.roomleveldata;
    roomleveldata.outputType = "Email";
    const Api = require('../../index.js').Api;
    Api.marginShortagePenalty(context, event, function(response){//margin shortage penalty api call
        mspApiResponseCallback(context, response, function(messageToUser) {//message to user
            options.data.marginShortageResponse = messageToUser;
            console.log("next_state", options.next_state);
            callback(options, event, context);
        });
    });
}

function mspApiResponseCallback (context, response, callback) {
	var roomleveldata = context.simpledb.roomleveldata;
    var data = {
        endpoint: "Margin Shortage Penalty",
        segment: "NA",
        period: roomleveldata.startDate + "-" + roomleveldata.endDate
    };
    const BotUtils = require('../../index.js').BotUtils;
    BotUtils.apiResponseHandler(context, response, data, function(messageToUser) {//log data
        callback(messageToUser);
    });
}
