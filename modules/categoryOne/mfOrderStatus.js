
module.exports.mforderstatusmenu = {
	mfOrderStatusOptions:mfOrderStatusOptions,
	getuserOptions:getuserOptions
};

function mfOrderStatusOptions(options, event, context, callback) {
    console.log("mfOrderStatusOptions called"); //print dp options [Holding statement, Transaction statement]
    const CONFIG = require('../../index.js').CONFIG;
    options.data.showmforder_options = CONFIG.CONSTANTS.BUTTONS.orderstatus;
    console.log("next_state", options.next_state);
    callback(options, event, context);
}
function getuserOptions(options,event,context,callback){
	console.log("getuserOptions for mforderstatus called");
	let roomData = context.simpledb.roomleveldata;
	let msg=event.message.toLowerCase();
    const CONFIG = require('../../index.js').CONFIG;
    if (msg.includes("lump sum")) {
		roomData.orderType = "FP";
	} else if (msg.includes("xsip")) {
		roomData.orderType = "XSIP";
	} else if (msg.includes("isip")) {
		roomData.orderType = "ISIP";
	} else if (msg.includes("sip")) {
		roomData.orderType = "SIP";
	} else if (msg.includes("mandate")) {
		roomData.orderType = "MN";
	}
	else{
		options.next_state="invalid_user_options";	
		options.data.invalid_user_options_msg=CONFIG.CONSTANTS.TEXT["invalidInput"];
		callback(options,event,context);
		return;
	 }
	const Api = require('../../index.js').Api;
	Api.orderstatus(context, event, function(response){
		var data = {
            endpoint: "MF Order status",
            segment: roomData.orderType,
            period: "NA"
        };
        const BotUtils = require('../../index.js').BotUtils;
        BotUtils.apiResponseHandler(context, response, data, function(messageToUser) {//log data and print response
            options.data.orderStatusResponse = messageToUser;
            options.next_state="valid_user_options";
            console.log("next_state", options.next_state);
			callback(options, event, context);
        });
	});
}