module.exports.portFolioNo = {
    askPortFolioNo:askPortFolioNo,
    userEntersPortFolioNo:userEntersPortFolioNo
};

function askPortFolioNo(options, event, context, callback){
    console.log("askPortFolioNo called");
    const CONFIG = require('../../index.js').CONFIG;
	options.data.portFolioNoQuestion = CONFIG.CONSTANTS.TEXT.mf_portfoliono;
	console.log("next_state", options.next_state);
	callback(options, event, context);
}

function userEntersPortFolioNo(options, event, context, callback){
    console.log("userEntersPortFolioNo called");
    var roomleveldata = context.simpledb.roomleveldata;
    roomleveldata.portFolioNo = event.message;
    const CONFIG = require('../../index.js').CONFIG;
    options.data.outputType = CONFIG.CONSTANTS.BUTTONS.out.replace("_out", "");
    options.next_state = "askOutputType";
    console.log("next_state", options.next_state);
    callback(options, event, context);
}

module.exports.outputType = {
    view:view,
    email:email
};

function view(options, event, context, callback){
	console.log("view called");
	var roomleveldata = context.simpledb.roomleveldata;
	roomleveldata.outputType = "Link";
    const Api = require('../../index.js').Api;
	Api.mutualFund(context, event, function(response){//mutual fund api call
		mutualFundApiResponseCallback(context, response, function(messageToUser) {//message to user
			options.data.mfResponse = messageToUser;
			console.log("next_state", options.next_state);
			callback(options, event, context);
		});
	});
}

function email(options, event, context, callback){
	console.log("email called");
	var roomleveldata = context.simpledb.roomleveldata;
	roomleveldata.outputType = "Email";
    const Api = require('../../index.js').Api;
	Api.mutualFund(context, event, function(response){//mutual fund api call
		mutualFundApiResponseCallback(context, response, function(messageToUser) {//message to user
			options.data.mfResponse = messageToUser;
			console.log("next_state", options.next_state);
			callback(options, event, context);
		});
	});
}

function mutualFundApiResponseCallback(context, response, callback) {
	var roomleveldata = context.simpledb.roomleveldata;
    var data = {
        endpoint: "Mutual Fund",
        segment: "NA",
        period: "NA"
    }
    const BotUtils = require('../../index.js').BotUtils;
    BotUtils.apiResponseHandler(context, response, data, function(messageToUser) {//log data
        callback(messageToUser);
    });
}