module.exports.period = {
    askPeriod:askPeriod,
    previousFy:previousFy,
    currentFy:currentFy,
    goBackToMainMenu:goBackToMainMenu
};

function askPeriod(options, event, context, callback){
    console.log("askPeriod called");
    const CONFIG = require('../../index.js').CONFIG;
	options.data.profitAndLossPeriod = CONFIG.CONSTANTS.BUTTONS.pnlsub.replace(/_invalid/g, "Profit and Loss statement");
	console.log("next_state", options.next_state);
	callback(options, event, context);
}

function previousFy(options, event, context, callback) {
    console.log("previousFy called");
    const Utils = require('../../index.js').Utils;
    var dateMsg = Utils.fyPeriod(context, event, "previous"); //previous FY date range
    const CONFIG = require('../../index.js').CONFIG;
    options.data.outputType = CONFIG.CONSTANTS.BUTTONS.out.replace(/_out/g, dateMsg);
    console.log("next_state", options.next_state);
    callback(options, event, context);
}

function currentFy(options, event, context, callback) {
    console.log("currentFy called");
    const Utils = require('../../index.js').Utils;
    var dateMsg = Utils.fyPeriod(context, event, "current"); //current FY date range
    const CONFIG = require('../../index.js').CONFIG;
    options.data.outputType = CONFIG.CONSTANTS.BUTTONS.out.replace(/_out/g, dateMsg);
    console.log("next_state", options.next_state);
    callback(options, event, context);
}

function goBackToMainMenu(options, event, context, callback) {
    console.log("goBackToMainMenu called"); //go back to main menu
    var roomleveldata = context.simpledb.roomleveldata;
    roomleveldata.menuOffset = 0;
    console.log("next_state", options.next_state);
    callback(options, event, context);
}

module.exports.outputType = {
    view:view,
    email:email
};

function view(options, event, context, callback){
	console.log("view called");
	var roomleveldata = context.simpledb.roomleveldata;
	roomleveldata.outputType = "Link";
    const Api = require('../../index.js').Api;
	Api.profitAndLossStatement(context, event, function(response){//profit and loss statement api call
		profitAndLossStatementApiResponseCallback(context, response, function(messageToUser) {//message to user
		
		    console.log("Profit and loss view resp",messageToUser);
			options.data.profitAndLossResponse = messageToUser;
			console.log("next_state", options.next_state);
			callback(options, event, context);
		});
	});
}

function email(options, event, context, callback){
	console.log("email called");
	var roomleveldata = context.simpledb.roomleveldata;
	roomleveldata.outputType = "Email";
    const Api = require('../../index.js').Api;
	Api.profitAndLossStatement(context, event, function(response){//profit and loss statement api call
		profitAndLossStatementApiResponseCallback(context, response, function(messageToUser) {//message to user
			options.data.profitAndLossResponse = messageToUser;
			console.log("next_state", options.next_state);
			callback(options, event, context);
		});
	});
}

function profitAndLossStatementApiResponseCallback(context, response, callback) {
	var roomleveldata = context.simpledb.roomleveldata;
    var data = {
        endpoint: "Profit And Loss Statement",
        segment: "NA",
        period: roomleveldata.startDate + "-" + roomleveldata.endDate
    }
    const BotUtils = require('../../index.js').BotUtils;
    BotUtils.apiResponseHandler(context, response, data, function(messageToUser) {//log data
        callback(messageToUser);
    });
}