module.exports.period = {
    askPeriod:askPeriod,
    previousFy:previousFy,
    enterPeriod:enterPeriod,
    userEntersPeriod:userEntersPeriod
};

function askPeriod(options, event, context, callback){
    console.log("askPeriod called");
    const CONFIG = require('../../index.js').CONFIG;
	options.data.askPeriodQuestion = CONFIG.CONSTANTS.BUTTONS.saudasub.replace(/_invalid/g, "Sauda Details");
	console.log("next_state", options.next_state);
	callback(options, event, context);
}

function previousFy(options, event, context, callback) {
    console.log("previousFy called");
    const Utils = require('../../index.js').Utils;
    var dateMsg = Utils.fyPeriod(context, event, "previous"); //previous FY date range
    const CONFIG = require('../../index.js').CONFIG;
    options.data.outputType = CONFIG.CONSTANTS.BUTTONS.out.replace(/_out/g, dateMsg);
    console.log("next_state", options.next_state);
    callback(options, event, context);
}

function enterPeriod(options, event, context, callback) {
    console.log("enterPeriod called");//print 'Enter custom period'
    const CONFIG = require('../../index.js').CONFIG;
    options.data.customSaudaPeriod = CONFIG.CONSTANTS.TEXT.customSaudaPeriod;
    console.log("next_state", options.next_state);
    callback(options, event, context);
}

function userEntersPeriod(options, event, context, callback) {
    console.log("userEntersPeriod called");
    var roomleveldata = context.simpledb.roomleveldata;
    var msg = event.message.toLowerCase();
    const CONFIG = require('../../index.js').CONFIG;
    if (msg.includes("to")) {
        msg = msg.replace(/ /g, "");
        var x = msg.split("to");
        var d1 = x[0];
        var d2 = x[1];
        const Utils = require('../../index.js').Utils;
        var t1 = Utils.parseDate(d1);
        var t2 = Utils.parseDate(d2);
        if (t1 && t2) {//if dates are valid
            var customPeriod = Utils.customPeriod(context, event, d1, d2);
            var validation = Utils.validateDate(new Date(customPeriod.da1).getTime(), new Date(customPeriod.da2).getTime());
            if (validation) {//if date range is valid i.e. dates entered are before current date.
                if (customPeriod.timeDiff > 0) {//if time difference in date range is positive
                    if (customPeriod.fyValidation) {//if financial year validation fails i.e. April 1 falls between start and end date then it is not valid date range
                        options.next_state = "invalidDate";
                        options.data.invalidDateMsg = CONFIG.CONSTANTS.TEXT.invalidDateFy;
                    } else {
                        if (customPeriod.diffDays >= 91) {//if days difference is more than 3 months
                            options.next_state = "invalidDate";
                            options.data.invalidDateMsg = CONFIG.CONSTANTS.TEXT.invalidDateSauda;
                        } else {//all validations passed
                            roomleveldata.startDate = customPeriod.da1;
                            roomleveldata.endDate = customPeriod.da2;
                            options.next_state = "validDate";
                            options.data.outputType = CONFIG.CONSTANTS.BUTTONS.out.replace(/_out/g, customPeriod.dateMsg);
                        }
                    }
                } else {
	            	options.next_state = "invalidDate";
	        		options.data.invalidDateMsg = CONFIG.CONSTANTS.TEXT.invalidDateSauda;
	            }
            } else {
            	options.next_state = "invalidDate";
        		options.data.invalidDateMsg = CONFIG.CONSTANTS.TEXT.invalidDateSauda;
            }
        } else {
            options.next_state = "invalidDate";
            options.data.invalidDateMsg = CONFIG.CONSTANTS.TEXT.invalidDateSauda;
        }
    } else {//if dates are invalid
    	options.next_state = "invalidDate";
        options.data.invalidDateMsg = CONFIG.CONSTANTS.TEXT.invalidDateSauda;
    }

    console.log("next_state", options.next_state);
    callback(options, event, context);
}

module.exports.outputType = {
    view:view,
    email:email
};

function view(options, event, context, callback){
	console.log("view called");
	var roomleveldata = context.simpledb.roomleveldata;
	roomleveldata.outputType = "Link";
    const Api = require('../../index.js').Api;
	Api.saudaDetail(context, event, function(response){//sauda detail api call
	console.log("RESPONSE---->",response);
		saudaDetailApiResponseCallback(context, response, function(messageToUser) {//message to user
			options.data.saudaDetailResponse = messageToUser;
			console.log("next_state", options.next_state);
			callback(options, event, context);
		});
	});
}

function email(options, event, context, callback){
	console.log("email called");
	var roomleveldata = context.simpledb.roomleveldata;
	roomleveldata.outputType = "Email";
    const Api = require('../../index.js').Api;
	Api.saudaDetail(context, event, function(response){//sauda detail api call
		saudaDetailApiResponseCallback(context, response, function(messageToUser) {//message to user
			options.data.saudaDetailResponse = messageToUser;
			console.log("next_state", options.next_state);
			callback(options, event, context);
		});
	});
}

function saudaDetailApiResponseCallback(context, response, callback) {
	var roomleveldata = context.simpledb.roomleveldata;
    var data = {
        endpoint: "Sauda Detail",
        segment: "NA",
        period: roomleveldata.startDate + "-" + roomleveldata.endDate
    }
    const BotUtils = require('../../index.js').BotUtils;
    BotUtils.apiResponseHandler(context, response, data, function(messageToUser) {//log data
        callback(messageToUser);
    });
}