module.exports.main = {
    setNextStateFromRole:setNextStateFromRole
};

function setNextStateFromRole(options, event, context, callback){
    console.log("setNextStateFromRole called");
    var roomleveldata = context.simpledb.roomleveldata;
    if (roomleveldata.role.toLowerCase().includes("client")) {
        options.next_state = "client";
    } else {
        options.next_state = "nonClient";
    }
    console.log("next_state", options.next_state);
    callback(options, event, context);
}

module.exports.clientCodes = {
	clientCodesApiCall:clientCodesApiCall,
	userSelectsClientCode:userSelectsClientCode
};

function clientCodesApiCall(options, event, context, callback){
	console.log("clientCodesApiCall called");
    const Api = require('../../index.js').Api;
	Api.categoryTwoClientCodes(context, event, function(response){
		const BotUtils = require('../../index.js').BotUtils;
		options.data.clientCodes = BotUtils.categoryTwoClientCodesToMessage(response);
		console.log("next_state", options.next_state);
		callback(options, event, context);
	});
}

function userSelectsClientCode(options, event, context, callback){
	console.log("userSelectsClientCode called");
	var roomleveldata = context.simpledb.roomleveldata;
	roomleveldata.oldclientid = roomleveldata.clientid;
	roomleveldata.clientid = event.message;//set user input as client id
	options.next_state = "userSelectedClientCode";
	console.log("next_state", options.next_state);
	callback(options, event, context);
}

module.exports.period = {
	setPeriodQuestion:setPeriodQuestion,
	userEntersPeriod:userEntersPeriod
};

function setPeriodQuestion(options, event, context, callback){
	console.log("setPeriodQuestion called");
	const CONFIG = require('../../index.js').CONFIG;
	var roomleveldata = context.simpledb.roomleveldata;
	options.data.askPeriod = "For " + ApiName(roomleveldata.endpoint) + ", " + CONFIG.CONSTANTS.TEXT.categoryTwoPeriod;
	console.log("next_state", options.next_state);
	callback(options, event, context);
}

function userEntersPeriod(options, event, context, callback) {
    console.log("userEntersPeriod called");
    var roomleveldata = context.simpledb.roomleveldata;
    var msg = event.message.toLowerCase();
    const CONFIG = require('../../index.js').CONFIG;
    if (msg.includes("to")) {
        msg = msg.replace(/ /g, "");
        var x = msg.split("to");
        var d1 = x[0];
        var d2 = x[1];
        const Utils = require('../../index.js').Utils;
        var t1 = Utils.parseDate(d1);
        var t2 = Utils.parseDate(d2);
        if (t1 && t2) {//if dates are valid
            var customPeriod = Utils.customPeriod(context, event, d1, d2);
            var validation = Utils.validateDate(new Date(customPeriod.da1).getTime(), new Date(customPeriod.da2).getTime());
            if (validation) {//if date range is valid i.e. dates entered are before current date.
                if (customPeriod.timeDiff > 0) {//if time difference in date range is positive
                    //all validations passed
                    roomleveldata.startDate = customPeriod.da1;
                    roomleveldata.endDate = customPeriod.da2;
                    options.next_state = "validDate";
                    options.data.outputType = CONFIG.CONSTANTS.BUTTONS.out.replace(/_out/g, customPeriod.dateMsg);
                } else {
	            	options.next_state = "invalidDate";
	        		options.data.invalidDateMsg = CONFIG.CONSTANTS.TEXT.invalidDate;
	            }
            } else {
            	options.next_state = "invalidDate";
        		options.data.invalidDateMsg = CONFIG.CONSTANTS.TEXT.invalidDate;
            }
        }
    } else {//if dates are invalid
    	options.next_state = "invalidDate";
        options.data.invalidDateMsg = CONFIG.CONSTANTS.TEXT.invalidDate;
    }

    console.log("next_state", options.next_state);
    callback(options, event, context);
}

function ApiName(endpoint) {
	switch (endpoint) {
		case "AccountPerformanceReport": return "Account Performance Report";
		case "CorporateActionReport": return "Corporate Action Report";
		case "DebitNotes": return "Debit Notes";
		case "CapitalMovement": return "Capital Movement";
		case "Dividendledger": return "Dividend ledger";
		case "IncomeandExpensesStatement": return "Income and Expenses Statement";
	}
}

module.exports.outputType = {
    view:view,
    email:email
};

function view(options, event, context, callback){
	console.log("view called");
	var roomleveldata = context.simpledb.roomleveldata;
	roomleveldata.outputType = "Link";
    const Api = require('../../index.js').Api;
	Api.categoryTwoApiCall(context, event, function(response){//category two api call
		categoryTwoApiCallback(context, response, function(messageToUser) {//message to user
			options.data.apiResponse = messageToUser;
			console.log("next_state", options.next_state);
			callback(options, event, context);
		});
	});
}

function email(options, event, context, callback){
	console.log("email called");
	var roomleveldata = context.simpledb.roomleveldata;
	roomleveldata.outputType = "Email";
    const Api = require('../../index.js').Api;
	Api.categoryTwoApiCall(context, event, function(response){//category two api call
		categoryTwoApiCallback(context, response, function(messageToUser) {//message to user
			options.data.apiResponse = messageToUser;
			console.log("next_state", options.next_state);
			callback(options, event, context);
		});
	});
}

function categoryTwoApiCallback(context, response, callback) {
	var roomleveldata = context.simpledb.roomleveldata;
    var data = {
        endpoint: ApiName(roomleveldata.endpoint),
        segment: "NA",
        period: roomleveldata.startDate + "-" + roomleveldata.endDate
    }
    const BotUtils = require('../../index.js').BotUtils;
    BotUtils.apiResponseHandler(context, response, data, function(messageToUser) {//log data
        callback(messageToUser);
    }, false);
}

module.exports.clientCode = {
    setClientCodeText: setClientCodeText,
    setUserEnteredClientCode: setUserEnteredClientCode
};

function setClientCodeText(options, event, context, callback){
    console.log("setClientCodeText called");//ask user for client code
    const CONFIG = require('../../index.js').CONFIG;
    options.data.askClientCode = CONFIG.CONSTANTS.TEXT.clientCode;
    console.log("next_state", options.next_state);
    callback(options, event, context);
}

function setUserEnteredClientCode(options, event, context, callback){
    console.log("setUserEnteredClientCode called");//set user entered client code
    var roomleveldata = context.simpledb.roomleveldata;
    var text = event.message;
    roomleveldata.clientid = text;
    options.next_state="go_forward";
    console.log("next_state", options.next_state);
    callback(options, event, context);
}

module.exports.quickLinks = {
    quickLinksApiCall:quickLinksApiCall
};

function quickLinksApiCall(options, event, context, callback) {
    console.log("quickLinksApiCall called"); //print quick links response
    var roomleveldata = context.simpledb.roomleveldata;
    roomleveldata.orderType = "1";
    const Api = require('../../index.js').Api;
    Api.quickLinks(context, event, function(response){//quick links api call
        var data = {
            endpoint: "Quick Links",
            segment: roomleveldata.orderType,
            period: "NA"
        };
        const BotUtils = require('../../index.js').BotUtils;
        BotUtils.apiResponseHandler(context, response, data, function(messageToUser) {//log data and print response
            options.data.quickLinksResponse = messageToUser;
            console.log("next_state", options.next_state);
            callback(options, event, context);
        }, false);
    });
}

module.exports.cmrReport = {
    cmrReportApiCall:cmrReportApiCall
};

function cmrReportApiCall(options, event, context, callback) {
    console.log("cmrReportApiCall called"); //print cmr report response
    const Api = require('../../index.js').Api;
    Api.cmrReportTwo(context, event, function(response){//cmr report api call
        var data = {
            endpoint: "CMR Report",
            segment: "NA",
            period: "NA"
        };
        const BotUtils = require('../../index.js').BotUtils;
        BotUtils.apiResponseHandler(context, response, data, function(messageToUser) {//log data and print response
            options.data.cmrReportResponse = messageToUser;
            console.log("next_state", options.next_state);
            callback(options, event, context);
        }, false);
    });
}

module.exports.clientDetails = {
    clientDetailsApiCall:clientDetailsApiCall
};

function clientDetailsApiCall(options, event, context, callback) {
    console.log("clientDetailsApiCall called"); //print cmr report response
    const Api = require('../../index.js').Api;
    Api.clientDetailsTwo(context, event, function(response){//cmr report api call
        var data = {
            endpoint: "Client Details",
            segment: "NA",
            period: "NA"
        };
        const BotUtils = require('../../index.js').BotUtils;
        BotUtils.apiResponseHandler(context, response, data, function(messageToUser) {//log data and print response
            options.data.clientDetailsResponse = messageToUser;
            console.log("next_state", options.next_state);
            callback(options, event, context);
        }, false);
    });
}

module.exports.ApiCall = {
	apiCall:apiCall	
};

function apiCall(options, event, context, callback) {
	console.log("apiCall called");
	var text = event.message;
	var roomleveldata = context.simpledb.roomleveldata;
	roomleveldata.params = roomleveldata.params.replace(roomleveldata.oldclientid, text);
	roomleveldata.clientid = text;
	const BotUtils = require('../../index.js').BotUtils;
	BotUtils.httpPost(context, roomleveldata.url, roomleveldata.params, roomleveldata.headers, function(response){
		var data = roomleveldata.logData;
        BotUtils.apiResponseHandler(context, response, data, function(messageToUser) {//log data and print response
            options.data.apiResponse = messageToUser;
            console.log("next_state", options.next_state);
            callback(options, event, context);
        });		
	});
}