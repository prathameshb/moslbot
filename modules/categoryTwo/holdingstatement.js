
module.exports.main = {

	enterdate: enterdate,
	get_date: get_date
}

function enterdate(options, event, context, callback) {
	var roomData=context.simpledb.roomleveldata;
	roomData.segment = "H";
	options.data.date = "For Holding Statement. Please enter the date in dd/mm/yyyy format";
	callback(options, event, context);

}

function get_date(options, event, context, callback) {
	var x = event.message;
	const utils = require('../../index.js').Utils;
	var t1 = utils.parseDate(x);
	var roomData=context.simpledb.roomleveldata;
	const config = require('../../index.js').CONFIG;
	if (t1) {
		var modifiedDate = x.split("/");
			
		var y1 = modifiedDate[0];
		var y2 = modifiedDate[1];
		var y3 = modifiedDate[2];
		da1 = y2 + "/" + y1 + "/" + y3;

		var date1 = new Date(da1);
		var validation = utils.validateDate(date1.getTime(), date1.getTime());
		console.log("log:validateDate at Holding Statement:" + validation);
		if (validation) {
			var startDate = date1;
			var startDateString = (startDate.getMonth() + 1) + "/" + startDate.getDate() + "/" + startDate.getFullYear();
			roomData.startDate = startDateString;
			roomData.endDate = startDateString;
			var dateMsg = " ";
		    dateMsg = config.CONSTANTS.TEXT.dp_note;
		    options.next_state="valid_date";
			options.data.valid_date_msg=config.CONSTANTS.BUTTONS.out.replace(/_out/g, dateMsg);
			callback(options,event,context);
		} else {
			options.next_state="invalid_date";
			options.data.invalid_date_msg=config.CONSTANTS.TEXT.invalidDateHolding;
			callback(options,event,context);
		}
	} else {
			options.next_state="invalid_date";
			options.data.invalid_date_msg=config.CONSTANTS.TEXT.invalidDateHolding;
			callback(options,event,context);
	}
}
module.exports.HoldingStatementOutputType={
	
	view:viewHoldingStatement,
	email:emailHoldingStatement
}


function viewHoldingStatement(options, event, context, callback){
	console.log("view called");
	var roomleveldata = context.simpledb.roomleveldata;
	roomleveldata.outputType = "Link";
    const Api = require('../../index.js').Api;
    Api.holdingStatement(context, event, function(response){//dp statement api call
    	dpApiResponseCallback(context, response, function(messageToUser) {//message to user
			options.data.holdingStatementResponse = messageToUser;
			console.log("next_state", options.next_state);
			callback(options, event, context);
		});
    });
}

function emailHoldingStatement(options, event, context, callback){
	console.log("email called");
	var roomleveldata = context.simpledb.roomleveldata;
	roomleveldata.outputType = "Email";
    const Api = require('../../index.js').Api;
    Api.holdingStatement(context, event, function(response){//dp statement api call
    	dpApiResponseCallback(context, response, function(messageToUser) {//message to user
			options.data.holdingStatementResponse = messageToUser;
			console.log("next_state", options.next_state);
			callback(options, event, context);
		});
    });
}
function dpApiResponseCallback(context, response, callback) {
	var roomleveldata = context.simpledb.roomleveldata;
    var data = {
        endpoint: "Holding Statement",
        segment: "Holding Statement",
        period: roomleveldata.startDate + "-" + roomleveldata.endDate
    }
    const BotUtils = require('../../index.js').BotUtils;
    BotUtils.apiResponseHandler(context, response, data, function(messageToUser) {//log data
        callback(messageToUser);
    });
}