module.exports.main = {
	setClientId: setClientId,
	routeUserCategory: routeUserCategory
};

function setClientId(options, event, context, callback) {
	console.log("setClientId called");
	var roomleveldata = context.simpledb.roomleveldata;
	var role = roomleveldata.role;
	if (role.toLowerCase().includes("client")) { //if role is client, then clientid is same as userid.
		roomleveldata.clientid = roomleveldata.userid;
	}
	options.next_state = "routeUserCategory";
	console.log("next_state", options.next_state);
	callback(options, event, context);
}

function routeUserCategory(options, event, context, callback) {
	console.log("routeUserCategory called");
	var roomleveldata = context.simpledb.roomleveldata;
	var category = roomleveldata.category;
	if (category.toLowerCase() == "one") {
		options.next_state = "categoryOne";
	} else if (category.toLowerCase() == "two") {
		options.next_state = "categoryTwo";
	}
	console.log("next_state", options.next_state);
	callback(options, event, context);
}

module.exports.categoryOne = {
	setHelloMsg: setHelloMsg,
	resetMenuOffset: resetMenuOffset,
	ledgerreport: setNextStateFromRole,
	ledgerStatement: setNextStateFromRole,
	dpStatement: setNextStateFromRole,
	sttCertificate: setNextStateFromRole,
	clientDetails: setNextStateFromRole,
	cmrReport: setNextStateFromRole,
	marginShortage: setNextStateFromRole,
	contractNote: setNextStateFromRole,
	accountClosure: setNextStateFromRole,
	mutualFund: setNextStateFromRole,
	profitAndLossStatement: setNextStateFromRole,
	clientDashboard: setNextStateFromRole,
	saudaDetail: setNextStateFromRole,
	fundPayoutStatus: setNextStateFromRole,
	fundTransferStatus: setNextStateFromRole,
	orderstatus: setNextStateFromRole,
	status: status,
	marginshotagepenalty: setNextStateFromRole,
	disbook: setNextStateFromRole,
	hps: setNextStateFromRole,
	accountstatus: setNextStateFromRole,
	newaccountopening: setNextStateFromRole,
	modificationform: modificationForm,
	formsandformat: formAndFormats,
	processnotechecklist: processNoteAndCheckList,
	ask_clientcode:setUserEnteredClientCode,
	formatCheckList:setNextStateFromRole,
	exceptionState:exceptionHandler

}

function exceptionHandler(options, event, context, callback){
	console.log('Exception Handler Called');
	// options.next_state = "client7";
	// callback(options, event, context);
	    var msg=event.message.toLowerCase().trim();
	    var roomleveldata = context.simpledb.roomleveldata;
	    const CONFIG = require('../index.js').CONFIG;
        var url = CONFIG.CONSTANTS.MENULIST_API;
        var params = "role=" + roomleveldata.role + "&category=" + roomleveldata.category + "&userid=" + roomleveldata.userid + "&islive=0";
        var headers = {
            "sessionid": roomleveldata.sessionid,
            "token": roomleveldata.token,
            "Content-Type": "application/x-www-form-urlencoded"
        };
	 const BotUtils = require('../index.js').BotUtils;
	 BotUtils.httpPost(context, url, params, headers, function(response) {
            try {
                response = JSON.parse(response);
                console.log("MENU LIST RESP",response);
            } catch (error) {
                console.log(error);
            }
        if(response.role=="client"){
        	
        	for(let i=0;i<response.menuList.length;i++){
        		
        		var keywords=response.menuList[i]["keys"].split(',');
        		for(let j=0;j<keywords.length;j++){
        			
        			if(msg.includes(keywords[j].toLowerCase().trim())){
        				
        				var label=response.menuList[i]["label"];
        				switch(label){
        					case "Quick Link":
        						  options.next_state="callquicklink";
        						  break;
        					case "DP Statement":
        						  options.next_state="client2"; 
        						  break;	  
        					case "STT Certificate":
        						  options.next_state="client3";
        						  break;
        					case "Ledger Report":
        					 	  options.next_state="client0";
        						  break;
        					case "Contract note":
        						  options.next_state="client13";
        						  break;
						  	case "My Details":
						  		  options.next_state="client4";
								  break;
						  	case "Available Margin":
						  		   options.next_state="client6";
								   break;
						  	case "IPO":
						  		   options.next_state="callipo";
						           break;
						  	case "CMR Report":
						  		   options.next_state="client5";
						           break;
						  	case "DIS/DRF Status":
						  		   options.next_state="calldisdrf";
						           break;
						  	case "Mutual fund":
						  		   options.next_state="client8";
						           break;
						  	case "Profit and loss statement":
						  		   options.next_state="client9";
						           break;
						  	case "My Dashboard":
						  		   options.next_state="client10";
						           break;
						    case "Sauda Detail":
						    	   options.next_state="client14";
						           break;
						    case "DIS Book Request":
						    	   options.next_state="client17";
						           break;
						    case "Format/Check List":
						    	   options.next_state="client21";
						           break; 
						    case "Margin Shortage Penalty":
						    	   options.next_state="client16";
						           break; 
						    case "Modification Status":
						    	   options.next_state="client19";
						           break; 
						    case "Funds Payout Status":
						    	   options.next_state="client11";
						           break;  
						    case "MF Order Status":
						    	   options.next_state="client15";
						           break;
						    default:
						            options.next_state="sorry_msg";
						            options.data.sorry_msg="{\"type\":\"survey\",\"question\":\"Sorry, I did not understand. Please ask again or click on Home for main menu.\",\"msgid\":\"default_message\",\"options\":[{\"type\":\"home_link\",\"message\":\"home\",\"title\":\"Home\"}]}";
						            break;
						           
        				}
        				
        				
        				
        			}
        			
        			
        		}
        		
        		
        	}
        	
        	
        }
        else{
        	
        	for(let i=0;i<response.menuList.length;i++){
        		
        		var keywords=response.menuList[i]["keys"].split(',');
        		for(let j=0;j<keywords.length;j++){
        			
        			if(msg.includes(keywords[j].toLowerCase().trim())){
        				
        				var label=response.menuList[i]["label"];
        				switch(label){
        					case "Quick Link":
        						  options.next_state="callquicklink";
        						  break;
        					case "DP Statement":
        						  options.next_state="nonClient2"; 
        						  break;	  
        					case "STT Certificate":
        						  options.next_state="nonClient3";
        						  break;
        					case "Ledger Report":
        					 	  options.next_state="nonClient0";
        						  break;
        					case "Contract note":
        						  options.next_state="nonClient13";
        						  break;
						  	case "My Details":
						  		  options.next_state="nonClient4";
								  break;
						  	case "Available Margin":
						  		   options.next_state="nonClient6";
								   break;
						  	case "IPO":
						  		   options.next_state="callipo";
						           break;
						  	case "CMR Report":
						  		   options.next_state="nonClient5";
						           break;
						  	case "DIS/DRF Status":
						  		   options.next_state="calldisdrf";
						           break;
						  	case "Mutual fund":
						  		   options.next_state="nonClient8";
						           break;
						  	case "Profit and loss statement":
						  		   options.next_state="nonClient9";
						           break;
						  	case "My Dashboard":
						  		   options.next_state="nonClient10";
						           break;
						    case "Sauda Detail":
						    	   options.next_state="nonClient14";
						           break;
						    case "DIS Book Request":
						    	   options.next_state="nonClient17";
						           break;
						    case "Format/Check List":
						    	   options.next_state="nonClient21";
						           break; 
						    case "Margin Shortage Penalty":
						    	   options.next_state="nonClient16";
						           break; 
						    case "Modification Status":
						    	   options.next_state="nonClient19";
						           break; 
						    case "Funds Payout Status":
						    	   options.next_state="nonClient11";
						           break;  
						    case "MF Order Status":
						    	   options.next_state="nonClient15";
						           break;
						    default:
						            options.next_state="sorry_msg";
						            options.data.sorry_msg="{\"type\":\"survey\",\"question\":\"Sorry, I did not understand. Please ask again or click on Home for main menu.\",\"msgid\":\"default_message\",\"options\":[{\"type\":\"home_link\",\"message\":\"home\",\"title\":\"Home\"}]}";
						            break;
						           
        				}
        				
        				
        				
        			}
        			
        			
        		}
        		
        		
        	}
        	
        	
        }
        
        callback(options,event,context);    
	 });
}
function setHelloMsg(options, event, context, callback) {
	console.log("setHelloMsg called");
	var roomleveldata = context.simpledb.roomleveldata;
	var welcomeMsg;
	const CONFIG = require('../index.js').CONFIG;
	if (!roomleveldata.menuOffset || roomleveldata.menuOffset == 0) {
		welcomeMsg = CONFIG.CONSTANTS.TEXT.welcomeCategoryOne;
	} else {
		welcomeMsg = CONFIG.CONSTANTS.TEXT.select;
	}
	const Utils = require('../index.js').Utils;
	var surveyOptions = Utils.sliceMenuList(context); //menuOffset is update inside this method.
	var survey = {
		"type": "survey",
		"question": welcomeMsg,
		"msgid": "welcomeMsg",
		"options": surveyOptions
	};
	options.next_state = "printHelloMsg";
	options.data.helloMsg = JSON.stringify(survey);
	console.log("next_state", options.next_state);
	callback(options, event, context);
}

function resetMenuOffset(options, event, context, callback) {
	console.log("resetMenuOffset called");
	var roomleveldata = context.simpledb.roomleveldata;
	roomleveldata.menuOffset = 0; //reset menuOffset to show first 5 options.
	console.log("next_state", options.next_state);
	callback(options, event, context);
}

function setNextStateFromRole(options, event, context, callback) {
	console.log("setNextStateFromRole called");
	var roomleveldata = context.simpledb.roomleveldata;
	var role = roomleveldata.role.toLowerCase();
	var next_states = options.getCurrentStateData().nextstates;
	if (role.toLowerCase().includes("client")) {
		options.next_state = next_states[0]; //client role state
	} else {
		options.next_state = next_states[1]; //non client role state
	}
	console.log("next_state", options.next_state);
	callback(options, event, context);
}

module.exports.categoryOne.ledger = ledger;

//module.exports.categoryOne.formchecklist=formCheckList;


// function formCheckList(options,event,context,callback){

// 	console.log("formCheckList called");
// 	if(options.data.formsandformat){
// 		formAndFormats(options,event,context,function(messageToUser){
// 				options.next_state="formandformats";
// 				options.data.api_resp=messageToUser;
// 				callback(options,event,context);
// 		});

// 	}
// 	else if(options.data.modificationform){
//         modificationForm(options,event,context,function(messageToUser){
// 			options.next_state="modificationform";
// 			options.data.api_resp=messageToUser;
// 			callback(options,event,context);	
// 		});
// 	}
// 	else if(options.data.processnotechecklist){
// 		processNoteAndCheckList(options,event,context,function(messageToUser){
// 			options.next_state="processnotechecklist";
// 			options.data.api_resp=messageToUser;
// 			callback(options,event,context);
// 		});	

// 	}
// 	else{

// 		options.next_state="commonformchecklist";
// 		callback(options,event,context,callback);


// 	}


// }
function modificationForm(options, event, context, callback) {
	console.log("modificationForm called");
	var roomleveldata = context.simpledb.roomleveldata;
	roomleveldata.documentType = "1";
	const Api = require('../index.js').Api;
	Api.formatCheckList(context, event, function(response) { //format check list api call
		formatCheckListApiResponseCallback(context, response, function(messageToUser) { //message to user

			options.data.api_resp = messageToUser;
			callback(options, event, context);
		});
	});
}

function formAndFormats(options, event, context, callback) {
	console.log("formAndFormats called");
	var roomleveldata = context.simpledb.roomleveldata;
	roomleveldata.documentType = "2";
	const Api = require('../index.js').Api;
	Api.formatCheckList(context, event, function(response) { //format check list api call
		formatCheckListApiResponseCallback(context, response, function(messageToUser) { //message to user

			console.log("next_state", options.next_state);
			options.data.api_resp = messageToUser;
			callback(options, event, context);
		});
	});
}

function processNoteAndCheckList(options, event, context, callback) {
	console.log("processNoteAndCheckList called");
	var roomleveldata = context.simpledb.roomleveldata;
	roomleveldata.documentType = "3";
	const Api = require('../index.js').Api;
	Api.formatCheckList(context, event, function(response) { //format check list api call
		formatCheckListApiResponseCallback(context, response, function(messageToUser) { //message to user
			console.log("next_state", options.next_state);
			options.data.api_resp = messageToUser;
			callback(options, event, context);
		});
	});
}

function formatCheckListApiResponseCallback(context, response, callback) {
	var roomleveldata = context.simpledb.roomleveldata;
	var data = {
		endpoint: "Format/Check List",
		segment: "NA",
		period: "NA"
	}
	const BotUtils = require('../index.js').BotUtils;
	BotUtils.apiResponseHandler(context, response, data, function(messageToUser) { //log data
		callback(messageToUser);
	}, false);
}

function ledger(options, event, context, callback) {
	console.log("ledger called");
	const CONFIG = require('../index.js').CONFIG;
	const Utils = require('../index.js').Utils;
	var roomleveldata = context.simpledb.roomleveldata;
	if (options.data.segment) {
		var segment = options.data.segment;
		switch (segment) {
			case "all segment":
				roomleveldata.segment = "All";
				roomleveldata.segmentTxt = "All segment";
				break;
			case "equity & cd":
			case "currency":
			case "equity":
				roomleveldata.segment = "EQ";
				roomleveldata.segmentTxt = "Equity & CD";
				break;
			case "commodity":
				roomleveldata.segment = "COM";
				roomleveldata.segmentTxt = "Commodity";
				break;
		}
	}
	if (options.data.period) {
		var period = options.data.period;
		switch (period) {
			case "3 months":
			case "3 month":
			case "last quarter":
			case "this quarter":
			case "three months":
			case "three month":
				roomleveldata.dateMsg = Utils.period(context, event, -91);
				break;
			case "6 months":
			case "6 month":
			case "six months":
			case "six month":
				roomleveldata.dateMsg = Utils.period(context, event, -183);
				break;
			case "2016":
			case "previous fy":
			case "previous financial year":
			case "previous year":
			case "last financial year":
			case "last year":
			case "last fy":	
				roomleveldata.dateMsg = Utils.fyPeriod(context, event, "previous");
				break;
			case "2017":
			case "2018":
			case "current fy":
			case "current financial year":
			case "this financial year":

			case "this year":
				roomleveldata.dateMsg = Utils.fyPeriod(context, event, "current");
				break;
		}
	}
	if (options.data.outputType) {
		var outputType = options.data.outputType;
		switch (outputType) {
			case "view":
			case "see":
			case "look":
			case "get":
			case "show":
			case "provide":
			case "give":
			case "need":	
				roomleveldata.outputType = "Link";
				break;
			case "mail":
			case "email":
				roomleveldata.outputType = "Email";
				break;
		}
	}
	if (!roomleveldata.segment || !roomleveldata.segmentTxt) {
		console.log("before ledger > refmsgid = ",event.messageobj.hasrefmsgid);
		if(!event.messageobj.hasrefmsgid){
			delete roomleveldata.clientid;
		}
		options.data.ledgerOptions = CONFIG.CONSTANTS.BUTTONS.ledger_without_go_back.replace(/_invalid/g, "Ledger Statement ");
		options.next_state = "askSegment";
	} else if (!roomleveldata.dateMsg) {
		options.data.ledgerDateOptions = CONFIG.CONSTANTS.BUTTONS.ledgersub_without_go_back.replace(/_invalid/g, "Ledger Statement->" + roomleveldata.segmentTxt);
		options.next_state = "askPeriod";
	} else if (!roomleveldata.outputType) {
		options.data.ledgerOutputType = CONFIG.CONSTANTS.BUTTONS.out_without_go_back.replace(/_out/g, roomleveldata.dateMsg);
		options.next_state = "askOutputType";
	} else {

		var roomleveldata = context.simpledb.roomleveldata;
		var role = roomleveldata.role.toLowerCase();
		if (!role.toLowerCase().includes("client") && !roomleveldata.clientid) {
			//ask client code
			options.next_state="ask_clientcode";
			const CONFIG = require('../index.js').CONFIG;
    		options.data.askClientCode = CONFIG.CONSTANTS.TEXT.clientCode;
			callback(options,event,context);
			return;

		} else {
			options.next_state = "success";
			const Api = require('../index.js').Api;
			Api.ledgerStatement(context, event, function(response) { //ledger statement api call
				ledgerApiResponseCallback(context, response, function(messageToUser) { //message to user
					options.data.ledgerResponse = messageToUser;
					//delete roomleveldata.segment;
					delete roomleveldata.segmentTxt;
					delete roomleveldata.dateMsg;
					delete roomleveldata.startDate;
					delete roomleveldata.endDate;
					delete roomleveldata.outputType;
					console.log("next_state", options.next_state);
					callback(options, event, context);
				});
			});
			return;
		}
	}
	console.log("next_state", options.next_state);
	callback(options, event, context);
}

function setUserEnteredClientCode(options, event, context, callback){
    console.log("setUserEnteredClientCode called");//set user entered client code
    var roomleveldata = context.simpledb.roomleveldata;
    roomleveldata.oldclientid = roomleveldata.clientid;
    var text = event.message;
    roomleveldata.clientid = text;
    const Api = require('../index.js').Api;
    Api.validateClientDetails(context, event, function(isValidClientId){
      	 if(isValidClientId){
      	 	options.next_state = "validClientId";
      	 	const Api = require('../index.js').Api;
			Api.ledgerStatement(context, event, function(response) { //ledger statement api call
				ledgerApiResponseCallback(context, response, function(messageToUser) { //message to user
					options.data.ledgerResponse = messageToUser;
				//	delete roomleveldata.segment;
					delete roomleveldata.segmentTxt;
					delete roomleveldata.dateMsg;
					delete roomleveldata.startDate;
					delete roomleveldata.endDate;
					delete roomleveldata.outputType;
					console.log("next_state", options.next_state);
					callback(options, event, context);
				});
			});
			return;
      	 } else {
      		options.next_state = "invalidClientId";	
      		options.data.invalidCodeMsg="Please enter a valid client code";
      		callback(options,event,context);
      	 }
      	 console.log("next_state", options.next_state);
    	 callback(options, event, context);
    });
}
function getSegment(segment) {
	segment = segment.toLowerCase();
	if (segment == "all") {
		return "All segment";
	} else if (segment == "eq") {
		return "Equity & CD";
	} else if (segment == "com") {
		return "Commodity";
	} else {
		return "NA";
	}
}

function ledgerApiResponseCallback(context, response, callback) {
	var roomleveldata = context.simpledb.roomleveldata;
	var data = {
		endpoint: "Ledger Statement",
		segment: getSegment(roomleveldata.segment),
		period: roomleveldata.startDate + "-" + roomleveldata.endDate
	}
	delete roomleveldata.segment
	const BotUtils = require('../index.js').BotUtils;
	BotUtils.apiResponseHandler(context, response, data, function(messageToUser) { //log data
		console.log("ledgerApiResponseCallback called for clientid="+roomleveldata.clientid,messageToUser);
		callback(messageToUser);
	});
}

function status(options, event, context, callback) {
	console.log("status called");
	let currentLabel = options.getCurrentStateData().label;
	console.log("currentLabel", currentLabel);
	if (currentLabel in options.data) {
		let nextStates = options.getCurrentStateData().nextstates;
		console.log("nextStates", nextStates);
		for (let nextState of nextStates) {
			let key = nextState.replace(`${currentLabel}_`, "");
			if (options.data[key] && options.data[key].length > 0) {
				options.next_state = nextState;
				break;
			}
		}
		if (options.next_state == -1) {
			options.next_state = `${currentLabel}_disambiguate`;
		}
	} else {
		options.next_state = `${currentLabel}_invalid`;
	}
	console.log("next_state", options.next_state);
	callback(options, event, context);
}

module.exports.categoryTwo = {
	setHelloMsg: setHelloMsg2,
	resetMenuOffset: resetMenuOffset2,
	accountperformancereport: setCategoryTwoApiEndpoint,
	corporateactionreport: setCategoryTwoApiEndpoint,
	debitnotes: setCategoryTwoApiEndpoint,
	capitalmovement: setCategoryTwoApiEndpoint,
	dividendledger: setCategoryTwoApiEndpoint,
	incomeandexpensesstatement: setCategoryTwoApiEndpoint,
	exceptionState:exceptionHandler2
};
function exceptionHandler2(options, event, context, callback){
	console.log('Exception Handler Called');
	// options.next_state = "client7";
	// callback(options, event, context);
	    var msg=event.message.toLowerCase().trim();
	    var roomleveldata = context.simpledb.roomleveldata;
	    const CONFIG = require('../index.js').CONFIG;
        var url = CONFIG.CONSTANTS.MENULIST_API;
        var params = "role=" + roomleveldata.role + "&category=" + roomleveldata.category + "&userid=" + roomleveldata.userid + "&islive=0";
        var headers = {
            "sessionid": roomleveldata.sessionid,
            "token": roomleveldata.token,
            "Content-Type": "application/x-www-form-urlencoded"
        };
	 const BotUtils = require('../index.js').BotUtils;
	 BotUtils.httpPost(context, url, params, headers, function(response) {
            try {
                response = JSON.parse(response);
                console.log("MENU LIST RESP",response);
            } catch (error) {
                console.log(error);
            }
       
        	for(let i=0;i<response.menuList.length;i++){
        		
        		var keywords=response.menuList[i]["keys"].split(',');
        		for(let j=0;j<keywords.length;j++){
        			
        			if(msg.includes(keywords[j].toLowerCase().trim())){
        				
        				var label=response.menuList[i]["label"];
        				switch(label){
        					case "Quick Link":
        						  options.next_state="callquicklink";
        						  break;
        					case "Client Details":
        						  options.next_state="client2"; 
        						  break;	  
        					case "CMR Report":
        						  options.next_state="client3";
        						  break;
        					case "Holding Statement":
        					 	  options.next_state="client0";
        						  break;
        					case "Transaction Statement":
        						  options.next_state="client13";
        						  break;
						  	case "Account Performance Report":
						  		  options.next_state="client4";
								  break;
						  	case "Corporate Action Report":
						  		   options.next_state="client6";
								   break;
						  	case "Debit Notes":
						  		   options.next_state="client7";
						           break;
						  	case "Capital Movement":
						  		   options.next_state="client5";
						           break;
						  	case "Dividend Ledger":
						  		   options.next_state="client9";
						           break;
						  	case "Income and Expenses Statement":
						  		   options.next_state="client8";
						           break;
						    default:
						            options.next_state="sorry_msg";
						            options.data.sorry_msg="{\"type\":\"survey\",\"question\":\"Sorry, I did not understand. Please ask again or click on Home for main menu.\",\"msgid\":\"default_message\",\"options\":[{\"type\":\"home_link\",\"message\":\"home\",\"title\":\"Home\"}]}";
						            break;
						           
        				}
        				
        				
        				
        			}
        			
        			
        		}
        		
        		
        	}
        	
        	
      
        
        callback(options,event,context);    
	 });
}
function setHelloMsg2(options, event, context, callback) {
	console.log("setHelloMsg called");
	var roomleveldata = context.simpledb.roomleveldata;
	var welcomeMsg;
	const CONFIG = require('../index.js').CONFIG;
	if (!roomleveldata.menuOffset || roomleveldata.menuOffset == 0) {
		welcomeMsg = CONFIG.CONSTANTS.TEXT.welcomeCategoryTwo;
	} else {
		welcomeMsg = CONFIG.CONSTANTS.TEXT.select;
	}
	const Utils = require('../index.js').Utils;
	var surveyOptions = Utils.sliceMenuList(context); //menuOffset is update inside this method.
	var survey = {
		"type": "survey",
		"question": welcomeMsg,
		"msgid": "welcomeMsg",
		"options": surveyOptions
	};
	options.next_state = "printHelloMsg";
	options.data.helloMsg = JSON.stringify(survey);
	console.log("next_state", options.next_state);
	callback(options, event, context);
}

function resetMenuOffset2(options, event, context, callback) {
	console.log("resetMenuOffset called");
	var roomleveldata = context.simpledb.roomleveldata;
	roomleveldata.menuOffset = 0; //reset menuOffset to show first 5 options.
	console.log("next_state", options.next_state);
	callback(options, event, context);
}

function setCategoryTwoApiEndpoint(options, event, context, callback) {
	console.log("setCategoryTwoApiEndpoint called");
	var state_label = options.getCurrentStateData().label;
	var roomleveldata = context.simpledb.roomleveldata;
	const CONFIG = require('../index.js').CONFIG;
	roomleveldata.endpoint = CONFIG.CONSTANTS.FUNCTIONS[state_label].endpoint;
	console.log("next_state", options.next_state);
	callback(options, event, context);

}