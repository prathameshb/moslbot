const _ = require('underscore');
const request=require('request');
const http = require('http');
const querystring = require('querystring');
const moment=require('moment');
function BotUtils() {

	var proto = this;

	proto.get = function(context, key, callback) {
		context.simpledb.doGet(key, function(ctx, evt) {
			var data = evt.dbval;
			try {
				data = JSON.parse(data);
			} catch (error) {
				// console.log(error);
			}
			return callback(data);
		});
	};

	proto.put = function(context, key, value, callback) {
		context.simpledb.doPut(key, value, callback);
	};

	proto.httpPost = function(context, url, params, headers, callback) {
		console.log("HTTP POST CALLED");
		console.log("url:" + url + "\nparams:" + params + "\nheader:" + JSON.stringify(headers));
		context.simplehttp.makePost(url, params, headers, function(ctx, evt) {
		    console.log("Response:",evt.getresp);
		    return callback(evt.getresp);
		});

	
	};

	proto.setCreds = function(context, key, params, callback) {
		var setCreds = {};
		var token = params.token;
		setCreds.token = token;
		var sessionid = params.sessionid;
		setCreds.sessionid = sessionid;
		var role = params.role;
		setCreds.role = role;
		var userid = params.userid;
		setCreds.userid = userid;
		var category = params.category || "";
		setCreds.category = category;
		var baseurl = params.baseurl;
		if (baseurl.endsWith("/") && category && category.trim().length > 0) {
			baseurl = baseurl + "api/" + (category + "/") || "";
		} else {
			baseurl = baseurl + "/api/" + (category + "/") || "";
		}
		setCreds.baseurl = baseurl;
		const Api = require('../index.js').Api;
		Api.getMenuList(context, key, setCreds, function(menuList) {
			if (menuList) { //pick 'label' field from menuList
				var labels = _.pluck(menuList, 'label');
				setCreds.labels = labels;
				console.log("doPut of ", key, " ", setCreds);
				proto.put(context, key, setCreds, callback);
			} else {
				callback();
			}
		});
	};

	proto.delCreds = function(context, key, callback) {
		proto.get(context, key, function(data) {
			if (data) {
				var setCreds = {};
				console.log("doPut of ", key, " ", setCreds);
				proto.put(context, key, setCreds, callback);
			} else {
				callback();
			}
		});
	};

	proto.apiResponseToBotMessage = function(roomleveldata, response, askAnotherClientCodeFlag) {
		const CONFIG = require('../index.js').CONFIG;
		try {
			console.log("apiResponseToBotMessage for clientid="+roomleveldata.clientid+" and outputtype="+roomleveldata.outputType,response);
			response = JSON.parse(response);
			if ("status" in response && "message" in response) {
				var status = response["status"];
				var message = response["message"];
				if (roomleveldata.outputType == "Link" || response["type"]=="Link") {
					if (!message.includes("a href") && message.includes("http")) { //hyperlink not present in api response then add a href tag
						message = "<a href =" + message + " target=_blank>" + message + "</a>";
					} else { //escape double quotes
						message = message.replace(/"/g, "\\\"");
					}
					console.log("LINK GENERATED",message);
				}
				if (status.toLowerCase() == "success" || status.toLowerCase() == "sucess") {
					if (roomleveldata.role.toLowerCase() == "client") {
						return {
							"status": "SUCCESS",
							"response": CONFIG.CONSTANTS.RESPONSES.apiresp.replace("_apiresp", message)
						};
					} else {
						if (!askAnotherClientCodeFlag) {
							return {
								"status": "SUCCESS",
								"response": CONFIG.CONSTANTS.RESPONSES.apiresp.replace("_apiresp", message)
							};
						} else {
							return {
								"status": "SUCCESS",
								"response": CONFIG.CONSTANTS.RESPONSES.apiRespNonClient.replace("_apiresp", message)
							};
						}
					}
				} else {
					if(roomleveldata.userid=="DEMO6265"){
						
						console.log("DEMO6265 CLIENT CALLED");
						console.log(CONFIG.CONSTANTS.RESPONSES.apiresp.replace("_apiresp", message))
					}
					console.log("CLIENT CODE ",roomleveldata.userid);
					if (roomleveldata.role.toLowerCase() == "client") {
						return {
							"status": "FAILURE",
							
							"response": CONFIG.CONSTANTS.RESPONSES.apiresp.replace("_apiresp", message)
						};
					} else {
						if (!askAnotherClientCodeFlag) {
							return {
								"status": "FAILURE",
								"response": CONFIG.CONSTANTS.RESPONSES.apiresp.replace("_apiresp", message)
							};
						} else {
							return {
								"status": "FAILURE",
								"response": CONFIG.CONSTANTS.RESPONSES.apiRespNonClient.replace("_apiresp", message)
							};
						}
					}
				}
			} else {
				if ("message" in response) {
					var message = response.message;
					return {
						"status": "FAILURE",
						"response": CONFIG.CONSTANTS.RESPONSES.apiresp.replace("_apiresp", message)
					};
				} else {
					return {
						"status": "FAILURE",
						"response": CONFIG.CONSTANTS.RESPONSES.apiresp.replace("_apiresp", CONFIG.CONSTANTS.RESPONSES.ERROR)
					};
				}
			}
		} catch (error) {
			console.log(error);
			return {
				"status": "FAILURE",
				"response": CONFIG.CONSTANTS.RESPONSES.apiresp.replace("_apiresp", CONFIG.CONSTANTS.RESPONSES.ERROR)
			};
		}
	};

	proto.dataAnalytics = function(context, key, value, callback) {
		proto.get(context, key, function(data) {
			data = data || [];
			if (!Array.isArray(data) || !data.length) { //push headers
				var dataFormat = ["User Id,Client Id,Role,Endpoint,Segment,Date range,Return Type,Keyword used,Response status,Bot response,Log time"];
				data = dataFormat;
			}
			data.push(value); //push log data into existing data array
			proto.put(context, key, data, callback);
		});
	};

	proto.logData = function(context, data, callback) {
		var roomleveldata = context.simpledb.roomleveldata;
		const Utils = require('../index.js').Utils;
		var currentDate = Utils.getCurrentDate(); //current date to store log data
		var logData = roomleveldata.userid + "," + (roomleveldata.clientid || roomleveldata.userid) + "," + roomleveldata.role + "," + data.endpoint +
			"," + data.segment + "," + (data.period || "NA") + "," +
			(roomleveldata.outputType || "NA") + "," + "" + "," + data.status + "," + JSON.parse(data.messageToUser).question.replace(/,/g, " ") +
			"," + new Date(); //setting up log data
		proto.dataAnalytics(context, currentDate, logData, callback);
	};

	proto.apiResponseHandler = function(context, response, data, callback, askAnotherClientCodeFlag) {
		if (askAnotherClientCodeFlag == undefined) {
			askAnotherClientCodeFlag = true;
		}
		var roomleveldata = context.simpledb.roomleveldata;
		roomleveldata.logData = data;
		console.log("response in apiResponseHandler", response);
		var responseObj = proto.apiResponseToBotMessage(roomleveldata, response, askAnotherClientCodeFlag);
		data.status = responseObj.status;
		data.messageToUser = responseObj.response;
		proto.logData(context, data, function() { //log data
			callback(data.messageToUser);
		});
	};

	proto.categoryTwoClientCodesToMessage = function(response) {
		var clientCodes = JSON.parse(response).message;
		var options = clientCodes.split(",");
		var survey = {
			"type": "survey",
			"question": "Please select a client code",
			"msgid": "getClientCode",
			"options": options
		}
		return JSON.stringify(survey);
	};

	proto.getPreviousDayLogData = function(context, callback) {
		const Utils = require('../index.js').Utils;
		var today = Utils.getCurrentDate("yesterday");
		proto.get(context, today, function(data) {
			data = data || [];
			var csvContent = "";
			for (var i = 0; i < data.length; i++) {
				csvContent += data[i] + "\n";
			}
			return callback(csvContent, today);
		});
	};

	proto.sendMailWithAttachment = function(context, callback) {
		proto.getPreviousDayLogData(context, function(csvContent, today) {
			const CONFIG = require('../index.js').CONFIG;

			var message_body = csvContent;
			var mime_boundary = new Date().getTime();
			var message = "\n\n--" + mime_boundary + "\r\n";
			message += 'Content-Type: text/csv;name="sheet_' + today + '.csv";method=REQUEST' + "\n";
			message += "Content-Transfer-Encoding: 8bit\n\n";
			message += message_body;

			var postData = querystring.stringify({
				method: "EMS_POST_CAMPAIGN",
				userid: CONFIG.CONSTANTS.MAIL_USERID,
				password: CONFIG.CONSTANTS.MAIL_PASSWORD,
				from: "MOSL",
				v: "1.1",
				name: "MOSL",
				recipients: encodeURI(CONFIG.ENV_CONFIG.EMAILIDS),
				subject: "MOSL MIS Details",
				content: message,
				content_type: "multipart/alternative; charset=UTF-8; boundary=" + mime_boundary
			});

			var options = {
				hostname: 'enterprise.webaroo.com',
				path: '/GatewayAPI/rest',
				method: 'POST',
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',
					'Content-Length': Buffer.byteLength(postData)
				}
			};

			var response = "";
			var responseCode;

			var req = http.request(options, function(res) {
				responseCode = res.statusCode;
				console.log('Email Status code: ' + responseCode);
				console.log('Email Headers: ' + JSON.stringify(res.headers));
				res.setEncoding('utf8');
				res.on('data', function(chunk) {
					console.log('Email Body: ' + chunk);
					response += chunk;
				});
				res.on('end', function() {
					return callback("success", response);
				});
			});

			req.on('error', function(e) {
				console.log('Email problem with request: ' + e.message);
				response = e.message;
				return callback("error", response);
			});

			req.write(postData);
			req.end();
		});
	};
	
	proto.storeFailureResponses=function(context,event,callback){
		
		console.log("storeFailureResponses called");
		var date=moment().format("DD-MM-YYYY");
		var db_key=date+"_F";
		context.simpledb.doGet(db_key,function(context,event){
		
		   let dateobj = event.dbval;
		   if(dateobj!=undefined && dateobj!=""){
		   
		     let prevDateobj=dateobj;
		     if(prevDateobj["nlpFailureResult"]!=undefined){
		     	
		     	let resultobj={
		     		"botmsg":context.simpledb.roomleveldata["prev_msg"],
		     		"usermsg":event.message 
		     		
		     	}
		       	
		       	prevDateobj["nlpFailureResult"].push(resultobj);
		       	
		       	context.simpledb.doPut(db_key,prevDateobj,function(context,event){
		       	
		       	    console.log("Nlpfailure data pushed");
		       	    callback(true);
		       		
		       	});
		     	
						     	
		     }
		     else{
		     	
		     	let nlpFailureResult=[];
		        let resultobj={
		     		"botmsg":context.simpledb.roomleveldata["prev_msg"],
		     		"usermsg":event.message 
		     		
		     	}	
		     	nlpFailureResult.push(resultobj);
		     	prevDateobj["nlpFailureResult"]=nlpFailureResult;
		     	
		     	context.simpledb.doPut(db_key,prevDateobj,function(context,event){
		       	
		       	    console.log("Nlpfailure data pushed");
		       	    callback(true);
		       		
		       	});
		     	
             	     	
			     }
		     
		     
		   	
		   }
		   else{
		   	
		   		let dateobj={
		     		"nlpFailureResult":[]
		     		
		     	}
		     	let resultobj={
		     		"botmsg":context.simpledb.roomleveldata["prev_msg"],
		     		"usermsg":event.message 
		     		
		     	}
		     	console.log("IN ELSE",db_key);
		     	dateobj["nlpFailureResult"].push(resultobj);
		     	
		     	context.simpledb.doPut(db_key,dateobj,function(context,event){
		       	
		       	    console.log("Nlpfailure data pushed");
		       	    callback(true);
		       		
		       	});
		   	
		   	
		   }
		 	
			
		});
		
		
		
	};
}

module.exports = new BotUtils();