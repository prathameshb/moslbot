function Utils() {

    var proto = this;

    proto.sliceMenuList = function(context) {
        var roomleveldata = context.simpledb.roomleveldata;
        if (!roomleveldata.menuOffset) {
            roomleveldata.menuOffset = 0;
        }
        var labels = roomleveldata.labels;
        if (roomleveldata.menuOffset > labels.length) {//reset menuOffset once it exceeds labels length
            roomleveldata.menuOffset = 0;
        }
        var returnMenuListArray = [];//push 5 buttons to survey
        for (let index = roomleveldata.menuOffset; index < roomleveldata.menuOffset + 5 && index < labels.length; index++) {
            returnMenuListArray.push(labels[index]);
        }
        roomleveldata.menuOffset += 5;
        if (roomleveldata.menuOffset > 0 && roomleveldata.menuOffset < labels.length) {//push see more button
            returnMenuListArray.push({"type": "load_more","message": "more","title": "See More"});
        }
        if (roomleveldata.menuOffset > 5) {//push home button
            returnMenuListArray.push({"type":"home_link","message":"home","title":"Home"});
        }
        return returnMenuListArray;
    };

    proto.getCurrentDate = function (param) {//get date in dd-mm-yyyy format
        var date;
        if (param == "yesterday") {
            var ydayEpoch = module.exports.addDays(-1);
            date = new Date(ydayEpoch);
        } else {
            date = new Date();    
        }
        
        var dd = date.getDate();
        var mm = date.getMonth()+1; //January is 0!
        
        var yyyy = date.getFullYear();
        if(dd<10){
            dd='0'+dd;
        } 
        if(mm<10){
            mm='0'+mm;
        } 
        var returnDate = dd+'-'+mm+'-'+yyyy;
        return returnDate;
    };

    proto.parseDate = function(str) {
        var t = str.match(/^(\d{1,2})\/(\d{1,2})\/(\d{4})$/);
        if (t !== null) {
            var d = +t[1],
                m = +t[2],
                y = +t[3];
            var date = new Date(y, m - 1, d);
            if (date.getFullYear() === y && date.getMonth() === m - 1) {
                return true;
            }
        }
        return false;
    };

    proto.addDays = function(n) {//add 'n' number of days to current date time
        var time = new Date().getTime();
        var changedDate = new Date(time + (n * 24 * 60 * 60 * 1000));
        var ans = new Date().setTime(changedDate.getTime());
        return ans;
    };

    proto.validateDate = function(date1, date2) {//validate date as date before current date time
        var now = new Date().getTime();
        var temp1 = now - date1;
        var temp2 = now - date2;
        return (temp1 > 0 && temp2 > 0);
    };

    proto.period = function(context, event, days) {//get period range i.e. last 3 months or 6 months
        var roomleveldata = context.simpledb.roomleveldata;
        var category = roomleveldata.category.toLowerCase();

        var endDate = new Date();
        var endDateString = (endDate.getMonth() + 1) + "/" + endDate.getDate() + "/" + endDate.getFullYear();
        var changedDate = module.exports.addDays(days);
        var startDate = new Date(changedDate);

        var startDateString;
        if (category == "two") {
            startDateString = (startDate.getMonth() + 1) + "/" + startDate.getDate() + "/" + startDate.getFullYear();
        } else {
            var financialYear = new Date(2017, 3, 1);//April 1st
            var financialYearEpoch = financialYear.getTime();
            var startEpoch = startDate.getTime();

            if (financialYearEpoch > startEpoch) {//if 90/180 days back crosses April 1st then start from April 1st
                startDate = financialYear;
                startDateString = "4/1/" + startDate.getFullYear();
            } else {
                startDateString = (startDate.getMonth() + 1) + "/" + startDate.getDate() + "/" + startDate.getFullYear();
            }

        }
        roomleveldata.startDate = startDateString;
        roomleveldata.endDate = endDateString;
        var printStartString = startDate.getDate() + "/" + (startDate.getMonth() + 1) + "/" + startDate.getFullYear();
        var printEndString = endDate.getDate() + "/" + (endDate.getMonth() + 1) + "/" + endDate.getFullYear();
        var dateMsg = "Start date:" + printStartString + "<br>End date:" + printEndString;
        return dateMsg;
    };

    proto.fyPeriod = function(context, event, fy) {//financial year date range
        var roomleveldata = context.simpledb.roomleveldata;
        var dateMsg = "";
        var startDateString, endDateString;
        var printStartString, printEndString;
        var endDate = new Date();
        var m = (endDate.getMonth() + 1);

        if (fy == "previous") {
            if (m >= 4) {//after April
                printStartString = "1/4/" + (endDate.getFullYear() - 1);
                printEndString = "31/3/" + endDate.getFullYear();
                startDateString = "4/1/" + (endDate.getFullYear() - 1);
                endDateString = "3/31/" + endDate.getFullYear();
            } else {
                printStartString = "1/4/" + (endDate.getFullYear() - 2);
                printEndString = "31/3/" + (endDate.getFullYear() - 1);
                startDateString = "4/1/" + (endDate.getFullYear() - 2);
                endDateString = "3/31/" + (endDate.getFullYear() - 1);
            }
            roomleveldata.startDate = startDateString;
            roomleveldata.endDate = endDateString;
            dateMsg = "Start date:" + printStartString + "<br>End date:" + printEndString;
            return dateMsg;
        } else if (fy == "current") {
            if (m >= 4) {//after April
                printStartString = "1/4/" + endDate.getFullYear();
                printEndString = endDate.getDate() + "/" + (endDate.getMonth() + 1) + "/" + endDate.getFullYear();

                startDateString = "4/1/" + endDate.getFullYear();
                endDateString = (endDate.getMonth() + 1) + "/" + endDate.getDate() + "/" + endDate.getFullYear();
            } else {
                printStartString = "1/4/" + (endDate.getFullYear() - 1);
                printEndString = endDate.getDate() + "/" + (endDate.getMonth() + 1) + "/" + endDate.getFullYear();

                startDateString = "4/1/" + (endDate.getFullYear() - 1);
                endDateString = (endDate.getMonth() + 1) + "/" + endDate.getDate() + "/" + endDate.getFullYear();
            }
            roomleveldata.startDate = startDateString;
            roomleveldata.endDate = endDateString;
            dateMsg = "Start date:" + printStartString + "<br>End date:" + printEndString;
            return dateMsg;
        }
    };

    proto.customPeriod = function(context, event, d1, d2) {
        var modifiedDate = d1.split("/");
        var y1 = modifiedDate[0];
        var y2 = modifiedDate[1];
        var y3 = modifiedDate[2];
        var da1 = y2 + "/" + y1 + "/" + y3;
        var fromDate = new Date(y3, parseInt(y2 - 1), y1);
        var fromDateMonth = parseInt(y2 - 1);
        var fromDateYear = y3;

        modifiedDate = d2.split("/");
        y1 = modifiedDate[0];
        y2 = modifiedDate[1];
        y3 = modifiedDate[2];
        var da2 = y2 + "/" + y1 + "/" + y3;
        var toDate = new Date(y3, parseInt(y2 - 1), y1);

        var date1 = new Date(da1);
        var date2 = new Date(da2);
        var timeDiff = date2.getTime() - date1.getTime();
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        var customPeriod = {};
        var check;
        if (fromDateMonth < 3) {//before April
            check = new Date(fromDateYear, 3, 1);//April 1st of fromDateYear
            customPeriod.fyValidation = (check >= fromDate && check <= toDate) ? true : false;
        } else {
            check = new Date(parseInt(fromDateYear) + 1, 3, 1);//April 1st of fromDateYear + 1
            customPeriod.fyValidation = (check >= fromDate && check <= toDate) ? true : false;
        }
        customPeriod.timeDiff = timeDiff;
        customPeriod.diffDays = diffDays;
        customPeriod.da1 = da1;
        customPeriod.da2 = da2;
        customPeriod.dateMsg = "Start date:" + da1 + "<br>End date:" + da2;
        return customPeriod;
    };
}

module.exports = new Utils();